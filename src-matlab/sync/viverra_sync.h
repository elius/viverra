/*
 * viverra_sync.h
 *
 * Code generation for model "viverra_sync".
 *
 * Model version              : 1.345
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Mon Jun 12 07:48:04 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_viverra_sync_h_
#define RTW_HEADER_viverra_sync_h_
#include <math.h>
#include <string.h>
#ifndef viverra_sync_COMMON_INCLUDES_
# define viverra_sync_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* viverra_sync_COMMON_INCLUDES_ */

#include "viverra_sync_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_atan2d_snf.h"
#include "rt_hypotd_snf.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetBlkStateChangeFlag
# define rtmGetBlkStateChangeFlag(rtm) ((rtm)->blkStateChange)
#endif

#ifndef rtmSetBlkStateChangeFlag
# define rtmSetBlkStateChangeFlag(rtm, val) ((rtm)->blkStateChange = (val))
#endif

#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm)  ((rtm)->contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm)         ((rtm)->contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val)    ((rtm)->contStates = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm)           ((rtm)->intgData)
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val)      ((rtm)->intgData = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm)               ((rtm)->odeF)
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val)          ((rtm)->odeF = (val))
#endif

#ifndef rtmGetPeriodicContStateIndices
# define rtmGetPeriodicContStateIndices(rtm) ((rtm)->periodicContStateIndices)
#endif

#ifndef rtmSetPeriodicContStateIndices
# define rtmSetPeriodicContStateIndices(rtm, val) ((rtm)->periodicContStateIndices = (val))
#endif

#ifndef rtmGetPeriodicContStateRanges
# define rtmGetPeriodicContStateRanges(rtm) ((rtm)->periodicContStateRanges)
#endif

#ifndef rtmSetPeriodicContStateRanges
# define rtmSetPeriodicContStateRanges(rtm, val) ((rtm)->periodicContStateRanges = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm)  ((rtm)->zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm)                 ((rtm)->derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val)            ((rtm)->derivs = (val))
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

/* Block signals for system '<S2>/MATLAB Function' */
typedef struct {
  real_T x_sd;                         /* '<S2>/MATLAB Function' */
  real_T x_sq;                         /* '<S2>/MATLAB Function' */
} B_MATLABFunction_viverra_sync_T;

/* Block signals (auto storage) */
typedef struct {
  real_T Uaf;                          /* '<Root>/filter_Ua' */
  boolean_T u;                         /* '<Root>/2' */
  boolean_T u_a;                       /* '<Root>/3' */
  B_MATLABFunction_viverra_sync_T sf_MATLABFunction1;/* '<S2>/MATLAB Function1' */
  B_MATLABFunction_viverra_sync_T sf_MATLABFunction;/* '<S2>/MATLAB Function' */
} B_viverra_sync_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Delay_DSTATE[2];              /* '<Root>/Delay' */
} DW_viverra_sync_T;

/* Continuous states (auto storage) */
typedef struct {
  real_T filter_Ua_CSTATE;             /* '<Root>/filter_Ua' */
  real_T filter_Ub_CSTATE;             /* '<Root>/filter_Ub' */
  real_T filter_Uc_CSTATE;             /* '<Root>/filter_Uc' */
  real_T Integratorslip_CSTATE;        /* '<S7>/Integrator [slip]' */
  real_T filter_Ia_CSTATE;             /* '<Root>/filter_Ia' */
  real_T filter_Ib_CSTATE;             /* '<Root>/filter_Ib' */
  real_T filter_Iact_CSTATE;           /* '<S3>/filter_Iact' */
  real_T filter_Uact_CSTATE;           /* '<S3>/filter_Uact' */
} X_viverra_sync_T;

/* State derivatives (auto storage) */
typedef struct {
  real_T filter_Ua_CSTATE;             /* '<Root>/filter_Ua' */
  real_T filter_Ub_CSTATE;             /* '<Root>/filter_Ub' */
  real_T filter_Uc_CSTATE;             /* '<Root>/filter_Uc' */
  real_T Integratorslip_CSTATE;        /* '<S7>/Integrator [slip]' */
  real_T filter_Ia_CSTATE;             /* '<Root>/filter_Ia' */
  real_T filter_Ib_CSTATE;             /* '<Root>/filter_Ib' */
  real_T filter_Iact_CSTATE;           /* '<S3>/filter_Iact' */
  real_T filter_Uact_CSTATE;           /* '<S3>/filter_Uact' */
} XDot_viverra_sync_T;

/* State disabled  */
typedef struct {
  boolean_T filter_Ua_CSTATE;          /* '<Root>/filter_Ua' */
  boolean_T filter_Ub_CSTATE;          /* '<Root>/filter_Ub' */
  boolean_T filter_Uc_CSTATE;          /* '<Root>/filter_Uc' */
  boolean_T Integratorslip_CSTATE;     /* '<S7>/Integrator [slip]' */
  boolean_T filter_Ia_CSTATE;          /* '<Root>/filter_Ia' */
  boolean_T filter_Ib_CSTATE;          /* '<Root>/filter_Ib' */
  boolean_T filter_Iact_CSTATE;        /* '<S3>/filter_Iact' */
  boolean_T filter_Uact_CSTATE;        /* '<S3>/filter_Uact' */
} XDis_viverra_sync_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState Integratorslip_Reset_ZCE; /* '<S7>/Integrator [slip]' */
} PrevZCX_viverra_sync_T;

/* Invariant block signals (auto storage) */
typedef struct {
  const real_T Gain;                   /* '<S7>/Gain' */
} ConstB_viverra_sync_T;

#ifndef ODE1_INTG
#define ODE1_INTG

/* ODE1 Integration Data */
typedef struct {
  real_T *f[1];                        /* derivatives */
} ODE1_IntgData;

#endif

/* External inputs (root inport signals with auto storage) */
typedef struct {
  real_T Ua;                           /* '<Root>/In3' */
  real_T Ub;                           /* '<Root>/In4' */
  real_T Uc;                           /* '<Root>/In5' */
  real_T Ia;                           /* '<Root>/Ia' */
  real_T Ib;                           /* '<Root>/In2' */
  real_T Rs;                           /* '<Root>/In1' */
} ExtU_viverra_sync_T;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  real_T Uabs;                         /* '<Root>/Uabs' */
  real_T Uang;                         /* '<Root>/Uang' */
  real_T Iabs;                         /* '<Root>/Iabs' */
  real_T phi;                          /* '<Root>/phi' */
  real_T Pact;                         /* '<Root>/Pact' */
  real_T Add1;                         /* '<Root>/Add1' */
  real_T Pm;                           /* '<Root>/Pm' */
  real_T Ireact;                       /* '<Root>/Ireact' */
  real_T Iact;                         /* '<Root>/Iact' */
  real_T Uact;                         /* '<Root>/Uact' */
} ExtY_viverra_sync_T;

/* Real-time Model Data Structure */
struct tag_RTM_viverra_sync_T {
  const char_T *errorStatus;
  RTWSolverInfo solverInfo;
  X_viverra_sync_T *contStates;
  int_T *periodicContStateIndices;
  real_T *periodicContStateRanges;
  real_T *derivs;
  boolean_T *contStateDisabled;
  boolean_T zCCacheNeedsReset;
  boolean_T derivCacheNeedsReset;
  boolean_T blkStateChange;
  real_T odeF[1][8];
  ODE1_IntgData intgData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Block signals (auto storage) */
extern B_viverra_sync_T viverra_sync_B;

/* Continuous states (auto storage) */
extern X_viverra_sync_T viverra_sync_X;

/* Block states (auto storage) */
extern DW_viverra_sync_T viverra_sync_DW;

/* External inputs (root inport signals with auto storage) */
extern ExtU_viverra_sync_T viverra_sync_U;

/* External outputs (root outports fed by signals with auto storage) */
extern ExtY_viverra_sync_T viverra_sync_Y;
extern const ConstB_viverra_sync_T viverra_sync_ConstB;/* constant block i/o */

/* Model entry point functions */
extern void viverra_sync_initialize(void);
extern void viverra_sync_step(void);
extern void viverra_sync_terminate(void);

/* Real-time Model object */
extern RT_MODEL_viverra_sync_T *const viverra_sync_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'viverra_sync'
 * '<S1>'   : 'viverra_sync/Emulate'
 * '<S2>'   : 'viverra_sync/FOC'
 * '<S3>'   : 'viverra_sync/FOC/AD_tok'
 * '<S4>'   : 'viverra_sync/FOC/Cartesian to Polar'
 * '<S5>'   : 'viverra_sync/FOC/MATLAB Function'
 * '<S6>'   : 'viverra_sync/FOC/MATLAB Function1'
 * '<S7>'   : 'viverra_sync/FOC/Scalar generator'
 * '<S8>'   : 'viverra_sync/FOC/AD_tok/thremal loss'
 */
#endif                                 /* RTW_HEADER_viverra_sync_h_ */
