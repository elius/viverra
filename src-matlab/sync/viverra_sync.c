/*
 * viverra_sync.c
 *
 * Code generation for model "viverra_sync".
 *
 * Model version              : 1.345
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Mon Jun 12 07:48:04 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "viverra_sync.h"
#include "viverra_sync_private.h"

/* Block signals (auto storage) */
B_viverra_sync_T viverra_sync_B;

/* Continuous states */
X_viverra_sync_T viverra_sync_X;

/* Block states (auto storage) */
DW_viverra_sync_T viverra_sync_DW;

/* Previous zero-crossings (trigger) states */
PrevZCX_viverra_sync_T viverra_sync_PrevZCX;

/* External inputs (root inport signals with auto storage) */
ExtU_viverra_sync_T viverra_sync_U;

/* External outputs (root outports fed by signals with auto storage) */
ExtY_viverra_sync_T viverra_sync_Y;

/* Real-time model */
RT_MODEL_viverra_sync_T viverra_sync_M_;
RT_MODEL_viverra_sync_T *const viverra_sync_M = &viverra_sync_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 8;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  viverra_sync_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; ++i) {
    x[i] += h * f0[i];
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/*
 * Output and update for atomic system:
 *    '<S2>/MATLAB Function'
 *    '<S2>/MATLAB Function1'
 */
void viverra_sync_MATLABFunction(real_T rtu_x_a, real_T rtu_x_b, real_T rtu_x_c,
  real_T rtu_sin_theta, real_T rtu_cos_theta, B_MATLABFunction_viverra_sync_T
  *localB)
{
  static const real_T a[6] = { 0.816496580927726, 0.0, -0.408248290463863,
    0.70710678118654746, -0.408248290463863, -0.70710678118654746 };

  real_T a_0[2];
  int32_T i;

  /* MATLAB Function 'FOC/MATLAB Function': '<S5>:1' */
  /* Clarke = (2/3)*[1 -1/2 -1/2; 0 sqrt(3)/2 -sqrt(3)/2]; */
  /* '<S5>:1:4' Clarke = sqrt(2/3)*[1 -1/2 -1/2; 0 sqrt(3)/2 -sqrt(3)/2]; */
  /* '<S5>:1:5' x_alphabeta = Clarke*[x_a x_b x_c]'; */
  /* '<S5>:1:7' Park = [cos_theta sin_theta; -sin_theta cos_theta]; */
  /* '<S5>:1:8' res = Park*x_alphabeta; */
  for (i = 0; i < 2; i++) {
    a_0[i] = a[i + 4] * rtu_x_c + (a[i + 2] * rtu_x_b + a[i] * rtu_x_a);
  }

  /* '<S5>:1:9' x_sd = res(1); */
  localB->x_sd = rtu_cos_theta * a_0[0] + rtu_sin_theta * a_0[1];

  /* '<S5>:1:10' x_sq = res(2); */
  localB->x_sq = -rtu_sin_theta * a_0[0] + rtu_cos_theta * a_0[1];
}

/* Model step function */
void viverra_sync_step(void)
{
  /* local block i/o variables */
  real_T rtb_filter_Ub;
  real_T rtb_filter_Uc;
  real_T rtb_Fcn;
  real_T rtb_Fcn1;
  real_T rtb_filter_Ia;
  real_T rtb_filter_Ib;
  real_T rtb_Sum6;
  boolean_T didZcEventOccur;
  real_T rtb_Product;
  real_T rtb_ComplextoMagnitudeAngle1_o1;
  if (rtmIsMajorTimeStep(viverra_sync_M)) {
    /* set solver stop time */
    if (!(viverra_sync_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&viverra_sync_M->solverInfo,
                            ((viverra_sync_M->Timing.clockTickH0 + 1) *
        viverra_sync_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&viverra_sync_M->solverInfo,
                            ((viverra_sync_M->Timing.clockTick0 + 1) *
        viverra_sync_M->Timing.stepSize0 + viverra_sync_M->Timing.clockTickH0 *
        viverra_sync_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(viverra_sync_M)) {
    viverra_sync_M->Timing.t[0] = rtsiGetT(&viverra_sync_M->solverInfo);
  }

  /* TransferFcn: '<Root>/filter_Ua' */
  viverra_sync_B.Uaf = 0.0;
  viverra_sync_B.Uaf += 2000.0 * viverra_sync_X.filter_Ua_CSTATE;

  /* TransferFcn: '<Root>/filter_Ub' */
  rtb_filter_Ub = 0.0;
  rtb_filter_Ub += 2000.0 * viverra_sync_X.filter_Ub_CSTATE;

  /* TransferFcn: '<Root>/filter_Uc' */
  rtb_filter_Uc = 0.0;
  rtb_filter_Uc += 2000.0 * viverra_sync_X.filter_Uc_CSTATE;
  if (rtmIsMajorTimeStep(viverra_sync_M)) {
    /* RelationalOperator: '<Root>/2' incorporates:
     *  Constant: '<Root>/0'
     *  Delay: '<Root>/Delay'
     */
    viverra_sync_B.u = (viverra_sync_DW.Delay_DSTATE[0] <= 0.0);
  }

  /* Logic: '<Root>/3' incorporates:
   *  Constant: '<Root>/0'
   *  RelationalOperator: '<Root>/1'
   */
  viverra_sync_B.u_a = (viverra_sync_B.u && (viverra_sync_B.Uaf > 0.0));

  /* Integrator: '<S7>/Integrator [slip]' */
  if (rtmIsMajorTimeStep(viverra_sync_M)) {
    didZcEventOccur = (((viverra_sync_PrevZCX.Integratorslip_Reset_ZCE ==
                         POS_ZCSIG) != (int32_T)viverra_sync_B.u_a) &&
                       (viverra_sync_PrevZCX.Integratorslip_Reset_ZCE !=
                        UNINITIALIZED_ZCSIG));
    viverra_sync_PrevZCX.Integratorslip_Reset_ZCE = viverra_sync_B.u_a;

    /* evaluate zero-crossings and the level of the reset signal */
    if (didZcEventOccur || viverra_sync_B.u_a) {
      viverra_sync_X.Integratorslip_CSTATE = -1.5707963267948966;
    }
  }

  /* Fcn: '<S2>/Fcn' incorporates:
   *  Integrator: '<S7>/Integrator [slip]'
   */
  rtb_Fcn = sin(viverra_sync_X.Integratorslip_CSTATE);

  /* Fcn: '<S2>/Fcn1' incorporates:
   *  Integrator: '<S7>/Integrator [slip]'
   */
  rtb_Fcn1 = cos(viverra_sync_X.Integratorslip_CSTATE);

  /* MATLAB Function: '<S2>/MATLAB Function1' */
  viverra_sync_MATLABFunction(viverra_sync_B.Uaf, rtb_filter_Ub, rtb_filter_Uc,
    rtb_Fcn, rtb_Fcn1, &viverra_sync_B.sf_MATLABFunction1);

  /* Outport: '<Root>/Uabs' incorporates:
   *  Fcn: '<S4>/x->r'
   */
  viverra_sync_Y.Uabs = rt_hypotd_snf(viverra_sync_B.sf_MATLABFunction1.x_sd,
    viverra_sync_B.sf_MATLABFunction1.x_sq);

  /* Outport: '<Root>/Uang' incorporates:
   *  Integrator: '<S7>/Integrator [slip]'
   */
  viverra_sync_Y.Uang = viverra_sync_X.Integratorslip_CSTATE;

  /* TransferFcn: '<Root>/filter_Ia' */
  rtb_filter_Ia = 0.0;
  rtb_filter_Ia += 2000.0 * viverra_sync_X.filter_Ia_CSTATE;

  /* TransferFcn: '<Root>/filter_Ib' */
  rtb_filter_Ib = 0.0;
  rtb_filter_Ib += 2000.0 * viverra_sync_X.filter_Ib_CSTATE;

  /* Sum: '<S2>/Sum6' */
  rtb_Sum6 = (0.0 - rtb_filter_Ia) - rtb_filter_Ib;

  /* MATLAB Function: '<S2>/MATLAB Function' */
  viverra_sync_MATLABFunction(rtb_filter_Ia, rtb_filter_Ib, rtb_Sum6, rtb_Fcn,
    rtb_Fcn1, &viverra_sync_B.sf_MATLABFunction);

  /* ComplexToMagnitudeAngle: '<S3>/Complex to Magnitude-Angle1' incorporates:
   *  RealImagToComplex: '<S3>/Real-Imag to Complex1'
   */
  rtb_ComplextoMagnitudeAngle1_o1 = rt_hypotd_snf
    (viverra_sync_B.sf_MATLABFunction.x_sd,
     viverra_sync_B.sf_MATLABFunction.x_sq);

  /* Outport: '<Root>/Iabs' */
  viverra_sync_Y.Iabs = rtb_ComplextoMagnitudeAngle1_o1;

  /* Outport: '<Root>/phi' incorporates:
   *  ComplexToMagnitudeAngle: '<S3>/Complex to Magnitude-Angle1'
   *  RealImagToComplex: '<S3>/Real-Imag to Complex1'
   */
  viverra_sync_Y.phi = rt_atan2d_snf(viverra_sync_B.sf_MATLABFunction.x_sq,
    viverra_sync_B.sf_MATLABFunction.x_sd);

  /* Product: '<S3>/Product' incorporates:
   *  TransferFcn: '<S3>/filter_Iact'
   *  TransferFcn: '<S3>/filter_Uact'
   */
  rtb_Product = 100.0 * viverra_sync_X.filter_Iact_CSTATE * (100.0 *
    viverra_sync_X.filter_Uact_CSTATE);

  /* Outport: '<Root>/Pact' */
  viverra_sync_Y.Pact = rtb_Product;

  /* Outport: '<Root>/Add1' */
  viverra_sync_Y.Add1 = viverra_sync_B.sf_MATLABFunction1.x_sq;

  /* Outport: '<Root>/Pm' incorporates:
   *  Inport: '<Root>/In1'
   *  MATLAB Function: '<S3>/thremal loss'
   *  Sum: '<S3>/Sum6'
   */
  /* MATLAB Function 'FOC/AD_tok/thremal loss': '<S8>:1' */
  /* '<S8>:1:2' P_therm = i*i*r; */
  viverra_sync_Y.Pm = rtb_Product - rtb_ComplextoMagnitudeAngle1_o1 *
    rtb_ComplextoMagnitudeAngle1_o1 * viverra_sync_U.Rs;

  /* Outport: '<Root>/Ireact' */
  viverra_sync_Y.Ireact = viverra_sync_B.sf_MATLABFunction.x_sq;

  /* Outport: '<Root>/Iact' */
  viverra_sync_Y.Iact = viverra_sync_B.sf_MATLABFunction.x_sd;

  /* Outport: '<Root>/Uact' */
  viverra_sync_Y.Uact = viverra_sync_B.sf_MATLABFunction1.x_sd;
  if (rtmIsMajorTimeStep(viverra_sync_M)) {
    if (rtmIsMajorTimeStep(viverra_sync_M)) {
      /* Update for Delay: '<Root>/Delay' */
      viverra_sync_DW.Delay_DSTATE[0] = viverra_sync_DW.Delay_DSTATE[1];
      viverra_sync_DW.Delay_DSTATE[1] = viverra_sync_B.Uaf;
    }
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(viverra_sync_M)) {
    rt_ertODEUpdateContinuousStates(&viverra_sync_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++viverra_sync_M->Timing.clockTick0)) {
      ++viverra_sync_M->Timing.clockTickH0;
    }

    viverra_sync_M->Timing.t[0] = rtsiGetSolverStopTime
      (&viverra_sync_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.0001s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.0001, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      viverra_sync_M->Timing.clockTick1++;
      if (!viverra_sync_M->Timing.clockTick1) {
        viverra_sync_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void viverra_sync_derivatives(void)
{
  XDot_viverra_sync_T *_rtXdot;
  _rtXdot = ((XDot_viverra_sync_T *) viverra_sync_M->derivs);

  /* Derivatives for TransferFcn: '<Root>/filter_Ua' incorporates:
   *  Derivatives for Inport: '<Root>/In3'
   */
  _rtXdot->filter_Ua_CSTATE = 0.0;
  _rtXdot->filter_Ua_CSTATE += -2000.0 * viverra_sync_X.filter_Ua_CSTATE;
  _rtXdot->filter_Ua_CSTATE += viverra_sync_U.Ua;

  /* Derivatives for TransferFcn: '<Root>/filter_Ub' incorporates:
   *  Derivatives for Inport: '<Root>/In4'
   */
  _rtXdot->filter_Ub_CSTATE = 0.0;
  _rtXdot->filter_Ub_CSTATE += -2000.0 * viverra_sync_X.filter_Ub_CSTATE;
  _rtXdot->filter_Ub_CSTATE += viverra_sync_U.Ub;

  /* Derivatives for TransferFcn: '<Root>/filter_Uc' incorporates:
   *  Derivatives for Inport: '<Root>/In5'
   */
  _rtXdot->filter_Uc_CSTATE = 0.0;
  _rtXdot->filter_Uc_CSTATE += -2000.0 * viverra_sync_X.filter_Uc_CSTATE;
  _rtXdot->filter_Uc_CSTATE += viverra_sync_U.Uc;

  /* Derivatives for Integrator: '<S7>/Integrator [slip]' */
  if (!viverra_sync_B.u_a) {
    _rtXdot->Integratorslip_CSTATE = viverra_sync_ConstB.Gain;
  } else {
    /* level reset is active */
    _rtXdot->Integratorslip_CSTATE = 0.0;
  }

  /* End of Derivatives for Integrator: '<S7>/Integrator [slip]' */

  /* Derivatives for TransferFcn: '<Root>/filter_Ia' incorporates:
   *  Derivatives for Inport: '<Root>/Ia'
   */
  _rtXdot->filter_Ia_CSTATE = 0.0;
  _rtXdot->filter_Ia_CSTATE += -2000.0 * viverra_sync_X.filter_Ia_CSTATE;
  _rtXdot->filter_Ia_CSTATE += viverra_sync_U.Ia;

  /* Derivatives for TransferFcn: '<Root>/filter_Ib' incorporates:
   *  Derivatives for Inport: '<Root>/In2'
   */
  _rtXdot->filter_Ib_CSTATE = 0.0;
  _rtXdot->filter_Ib_CSTATE += -2000.0 * viverra_sync_X.filter_Ib_CSTATE;
  _rtXdot->filter_Ib_CSTATE += viverra_sync_U.Ib;

  /* Derivatives for TransferFcn: '<S3>/filter_Iact' */
  _rtXdot->filter_Iact_CSTATE = 0.0;
  _rtXdot->filter_Iact_CSTATE += -100.0 * viverra_sync_X.filter_Iact_CSTATE;
  _rtXdot->filter_Iact_CSTATE += viverra_sync_B.sf_MATLABFunction.x_sd;

  /* Derivatives for TransferFcn: '<S3>/filter_Uact' */
  _rtXdot->filter_Uact_CSTATE = 0.0;
  _rtXdot->filter_Uact_CSTATE += -100.0 * viverra_sync_X.filter_Uact_CSTATE;
  _rtXdot->filter_Uact_CSTATE += viverra_sync_B.sf_MATLABFunction1.x_sd;
}

/* Model initialize function */
void viverra_sync_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)viverra_sync_M, 0,
                sizeof(RT_MODEL_viverra_sync_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&viverra_sync_M->solverInfo,
                          &viverra_sync_M->Timing.simTimeStep);
    rtsiSetTPtr(&viverra_sync_M->solverInfo, &rtmGetTPtr(viverra_sync_M));
    rtsiSetStepSizePtr(&viverra_sync_M->solverInfo,
                       &viverra_sync_M->Timing.stepSize0);
    rtsiSetdXPtr(&viverra_sync_M->solverInfo, &viverra_sync_M->derivs);
    rtsiSetContStatesPtr(&viverra_sync_M->solverInfo, (real_T **)
                         &viverra_sync_M->contStates);
    rtsiSetNumContStatesPtr(&viverra_sync_M->solverInfo,
      &viverra_sync_M->Sizes.numContStates);
    rtsiSetNumPeriodicContStatesPtr(&viverra_sync_M->solverInfo,
      &viverra_sync_M->Sizes.numPeriodicContStates);
    rtsiSetPeriodicContStateIndicesPtr(&viverra_sync_M->solverInfo,
      &viverra_sync_M->periodicContStateIndices);
    rtsiSetPeriodicContStateRangesPtr(&viverra_sync_M->solverInfo,
      &viverra_sync_M->periodicContStateRanges);
    rtsiSetErrorStatusPtr(&viverra_sync_M->solverInfo, (&rtmGetErrorStatus
      (viverra_sync_M)));
    rtsiSetRTModelPtr(&viverra_sync_M->solverInfo, viverra_sync_M);
  }

  rtsiSetSimTimeStep(&viverra_sync_M->solverInfo, MAJOR_TIME_STEP);
  viverra_sync_M->intgData.f[0] = viverra_sync_M->odeF[0];
  viverra_sync_M->contStates = ((X_viverra_sync_T *) &viverra_sync_X);
  rtsiSetSolverData(&viverra_sync_M->solverInfo, (void *)
                    &viverra_sync_M->intgData);
  rtsiSetSolverName(&viverra_sync_M->solverInfo,"ode1");
  rtmSetTPtr(viverra_sync_M, &viverra_sync_M->Timing.tArray[0]);
  viverra_sync_M->Timing.stepSize0 = 0.0001;

  /* block I/O */
  (void) memset(((void *) &viverra_sync_B), 0,
                sizeof(B_viverra_sync_T));

  /* states (continuous) */
  {
    (void) memset((void *)&viverra_sync_X, 0,
                  sizeof(X_viverra_sync_T));
  }

  /* states (dwork) */
  (void) memset((void *)&viverra_sync_DW, 0,
                sizeof(DW_viverra_sync_T));

  /* external inputs */
  (void)memset((void *)&viverra_sync_U, 0, sizeof(ExtU_viverra_sync_T));

  /* external outputs */
  (void) memset((void *)&viverra_sync_Y, 0,
                sizeof(ExtY_viverra_sync_T));
  viverra_sync_PrevZCX.Integratorslip_Reset_ZCE = UNINITIALIZED_ZCSIG;

  /* InitializeConditions for TransferFcn: '<Root>/filter_Ua' */
  viverra_sync_X.filter_Ua_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/filter_Ub' */
  viverra_sync_X.filter_Ub_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/filter_Uc' */
  viverra_sync_X.filter_Uc_CSTATE = 0.0;

  /* InitializeConditions for Delay: '<Root>/Delay' */
  viverra_sync_DW.Delay_DSTATE[0] = 0.0;
  viverra_sync_DW.Delay_DSTATE[1] = 0.0;

  /* InitializeConditions for Integrator: '<S7>/Integrator [slip]' */
  viverra_sync_X.Integratorslip_CSTATE = -1.5707963267948966;

  /* InitializeConditions for TransferFcn: '<Root>/filter_Ia' */
  viverra_sync_X.filter_Ia_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<Root>/filter_Ib' */
  viverra_sync_X.filter_Ib_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<S3>/filter_Iact' */
  viverra_sync_X.filter_Iact_CSTATE = 0.0;

  /* InitializeConditions for TransferFcn: '<S3>/filter_Uact' */
  viverra_sync_X.filter_Uact_CSTATE = 0.0;
}

/* Model terminate function */
void viverra_sync_terminate(void)
{
  /* (no terminate code required) */
}
