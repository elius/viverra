/*
 *  rtmodel.h:
 *
 * Code generation for model "viverra_sync".
 *
 * Model version              : 1.345
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Mon Jun 12 07:48:04 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_rtmodel_h_
#define RTW_HEADER_rtmodel_h_

/*
 *  Includes the appropriate headers when we are using rtModel
 */
#include "viverra_sync.h"
#define GRTINTERFACE                   0
#endif                                 /* RTW_HEADER_rtmodel_h_ */
