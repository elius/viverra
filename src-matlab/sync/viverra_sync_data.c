/*
 * viverra_sync_data.c
 *
 * Code generation for model "viverra_sync".
 *
 * Model version              : 1.345
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Mon Jun 12 07:48:04 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "viverra_sync.h"
#include "viverra_sync_private.h"

/* Invariant block signals (auto storage) */
const ConstB_viverra_sync_T viverra_sync_ConstB = {
  314.15926535897933                   /* '<S7>/Gain' */
};
