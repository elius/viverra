/*
 * viverra_sync_private.h
 *
 * Code generation for model "viverra_sync".
 *
 * Model version              : 1.345
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Mon Jun 12 07:48:04 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_viverra_sync_private_h_
#define RTW_HEADER_viverra_sync_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "viverra_sync.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmIsMajorTimeStep
# define rtmIsMajorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP)
#endif

#ifndef rtmIsMinorTimeStep
# define rtmIsMinorTimeStep(rtm)       (((rtm)->Timing.simTimeStep) == MINOR_TIME_STEP)
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               ((rtm)->Timing.t)
#endif

#ifndef rtmSetTPtr
# define rtmSetTPtr(rtm, val)          ((rtm)->Timing.t = (val))
#endif

extern void viverra_sync_MATLABFunction(real_T rtu_x_a, real_T rtu_x_b, real_T
  rtu_x_c, real_T rtu_sin_theta, real_T rtu_cos_theta,
  B_MATLABFunction_viverra_sync_T *localB);

/* private model entry point functions */
extern void viverra_sync_derivatives(void);

#endif                                 /* RTW_HEADER_viverra_sync_private_h_ */
