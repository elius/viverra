/*
 * viverra_async_types.h
 *
 * Code generation for model "viverra_async".
 *
 * Model version              : 1.391
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Mon Jun 12 07:46:43 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_viverra_async_types_h_
#define RTW_HEADER_viverra_async_types_h_

/* Forward declaration for rtModel */
typedef struct tag_RTM_viverra_async_T RT_MODEL_viverra_async_T;

#endif                                 /* RTW_HEADER_viverra_async_types_h_ */
