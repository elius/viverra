/*
 * viverra_async.c
 *
 * Code generation for model "viverra_async".
 *
 * Model version              : 1.391
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Mon Jun 12 07:46:43 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "viverra_async.h"
#include "viverra_async_private.h"

/* Block signals (auto storage) */
B_viverra_async_T viverra_async_B;

/* Block states (auto storage) */
DW_viverra_async_T viverra_async_DW;

/* External inputs (root inport signals with auto storage) */
ExtU_viverra_async_T viverra_async_U;

/* External outputs (root outports fed by signals with auto storage) */
ExtY_viverra_async_T viverra_async_Y;

/* Real-time model */
RT_MODEL_viverra_async_T viverra_async_M_;
RT_MODEL_viverra_async_T *const viverra_async_M = &viverra_async_M_;

/* Model step function */
void viverra_async_step(void)
{
  real_T rtb_Product[3];
  real_T rtb_xtheta_f;
  real_T rtb_Product1[3];
  int32_T i;
  real_T rtb_Iin_idx_2;
  real_T rtb_xtheta_a;

  /* S-Function (sdspbiquad): '<Root>/Filter' incorporates:
   *  Inport: '<Root>/In3'
   */
  viverra_async_DW.Filter_TEMP_STATES[0] = (0.0076291496985537149 *
    viverra_async_U.Ua - -1.8648062707899464 *
    viverra_async_DW.Filter_FILT_STATES[0]) - 0.8953228695841613 *
    viverra_async_DW.Filter_FILT_STATES[1];
  viverra_async_DW.Filter_TEMP_STATES[1] = (((2.0 *
    viverra_async_DW.Filter_FILT_STATES[0] +
    viverra_async_DW.Filter_TEMP_STATES[0]) +
    viverra_async_DW.Filter_FILT_STATES[1]) * 0.0070335126208999361 -
    -1.7192136685456447 * viverra_async_DW.Filter_FILT_STATES[2]) -
    0.74734771902924446 * viverra_async_DW.Filter_FILT_STATES[3];
  viverra_async_DW.Filter_TEMP_STATES[2] = (((2.0 *
    viverra_async_DW.Filter_FILT_STATES[2] +
    viverra_async_DW.Filter_TEMP_STATES[1]) +
    viverra_async_DW.Filter_FILT_STATES[3]) * 0.082642823479589539 -
    -0.83471435304082109 * viverra_async_DW.Filter_FILT_STATES[4]) - 0.0 *
    viverra_async_DW.Filter_FILT_STATES[5];
  viverra_async_B.Uin[0] = (viverra_async_DW.Filter_TEMP_STATES[2] +
    viverra_async_DW.Filter_FILT_STATES[4]) + 0.0 *
    viverra_async_DW.Filter_FILT_STATES[5];

  /* S-Function (sdspbiquad): '<Root>/Filter1' incorporates:
   *  Inport: '<Root>/In4'
   */
  viverra_async_DW.Filter1_TEMP_STATES[0] = (0.0076291496985537149 *
    viverra_async_U.Ub - -1.8648062707899464 *
    viverra_async_DW.Filter1_FILT_STATES[0]) - 0.8953228695841613 *
    viverra_async_DW.Filter1_FILT_STATES[1];
  viverra_async_DW.Filter1_TEMP_STATES[1] = (((2.0 *
    viverra_async_DW.Filter1_FILT_STATES[0] +
    viverra_async_DW.Filter1_TEMP_STATES[0]) +
    viverra_async_DW.Filter1_FILT_STATES[1]) * 0.0070335126208999361 -
    -1.7192136685456447 * viverra_async_DW.Filter1_FILT_STATES[2]) -
    0.74734771902924446 * viverra_async_DW.Filter1_FILT_STATES[3];
  viverra_async_DW.Filter1_TEMP_STATES[2] = (((2.0 *
    viverra_async_DW.Filter1_FILT_STATES[2] +
    viverra_async_DW.Filter1_TEMP_STATES[1]) +
    viverra_async_DW.Filter1_FILT_STATES[3]) * 0.082642823479589539 -
    -0.83471435304082109 * viverra_async_DW.Filter1_FILT_STATES[4]) - 0.0 *
    viverra_async_DW.Filter1_FILT_STATES[5];
  viverra_async_B.Uin[1] = (viverra_async_DW.Filter1_TEMP_STATES[2] +
    viverra_async_DW.Filter1_FILT_STATES[4]) + 0.0 *
    viverra_async_DW.Filter1_FILT_STATES[5];

  /* S-Function (sdspbiquad): '<Root>/Filter2' incorporates:
   *  Inport: '<Root>/In5'
   */
  viverra_async_DW.Filter2_TEMP_STATES[0] = (0.0076291496985537149 *
    viverra_async_U.Uc - -1.8648062707899464 *
    viverra_async_DW.Filter2_FILT_STATES[0]) - 0.8953228695841613 *
    viverra_async_DW.Filter2_FILT_STATES[1];
  viverra_async_DW.Filter2_TEMP_STATES[1] = (((2.0 *
    viverra_async_DW.Filter2_FILT_STATES[0] +
    viverra_async_DW.Filter2_TEMP_STATES[0]) +
    viverra_async_DW.Filter2_FILT_STATES[1]) * 0.0070335126208999361 -
    -1.7192136685456447 * viverra_async_DW.Filter2_FILT_STATES[2]) -
    0.74734771902924446 * viverra_async_DW.Filter2_FILT_STATES[3];
  viverra_async_DW.Filter2_TEMP_STATES[2] = (((2.0 *
    viverra_async_DW.Filter2_FILT_STATES[2] +
    viverra_async_DW.Filter2_TEMP_STATES[1]) +
    viverra_async_DW.Filter2_FILT_STATES[3]) * 0.082642823479589539 -
    -0.83471435304082109 * viverra_async_DW.Filter2_FILT_STATES[4]) - 0.0 *
    viverra_async_DW.Filter2_FILT_STATES[5];
  viverra_async_B.Uin[2] = (viverra_async_DW.Filter2_TEMP_STATES[2] +
    viverra_async_DW.Filter2_FILT_STATES[4]) + 0.0 *
    viverra_async_DW.Filter2_FILT_STATES[5];

  /* Product: '<Root>/Product' incorporates:
   *  Constant: '<Root>/ClarkeP'
   */
  for (i = 0; i < 3; i++) {
    rtb_Product[i] = rtCP_ClarkeP_Value[i + 6] * viverra_async_B.Uin[2] +
      (rtCP_ClarkeP_Value[i + 3] * viverra_async_B.Uin[1] + rtCP_ClarkeP_Value[i]
       * viverra_async_B.Uin[0]);
  }

  /* End of Product: '<Root>/Product' */

  /* Outport: '<Root>/Uabs' incorporates:
   *  Fcn: '<S1>/x->r'
   */
  viverra_async_Y.Uabs = rt_hypotd_snf(rtb_Product[0], rtb_Product[1]);

  /* Fcn: '<S1>/x->theta' */
  rtb_xtheta_f = rt_atan2d_snf(rtb_Product[1], rtb_Product[0]);

  /* Outport: '<Root>/Uang' */
  viverra_async_Y.Uang = rtb_xtheta_f;

  /* S-Function (sdspbiquad): '<Root>/Filter3' incorporates:
   *  Inport: '<Root>/Ia'
   */
  viverra_async_DW.Filter3_TEMP_STATES[0] = (0.0076291496985537149 *
    viverra_async_U.Ia - -1.8648062707899464 *
    viverra_async_DW.Filter3_FILT_STATES[0]) - 0.8953228695841613 *
    viverra_async_DW.Filter3_FILT_STATES[1];
  viverra_async_DW.Filter3_TEMP_STATES[1] = (((2.0 *
    viverra_async_DW.Filter3_FILT_STATES[0] +
    viverra_async_DW.Filter3_TEMP_STATES[0]) +
    viverra_async_DW.Filter3_FILT_STATES[1]) * 0.0070335126208999361 -
    -1.7192136685456447 * viverra_async_DW.Filter3_FILT_STATES[2]) -
    0.74734771902924446 * viverra_async_DW.Filter3_FILT_STATES[3];
  viverra_async_DW.Filter3_TEMP_STATES[2] = (((2.0 *
    viverra_async_DW.Filter3_FILT_STATES[2] +
    viverra_async_DW.Filter3_TEMP_STATES[1]) +
    viverra_async_DW.Filter3_FILT_STATES[3]) * 0.082642823479589539 -
    -0.83471435304082109 * viverra_async_DW.Filter3_FILT_STATES[4]) - 0.0 *
    viverra_async_DW.Filter3_FILT_STATES[5];
  viverra_async_B.Filter3 = (viverra_async_DW.Filter3_TEMP_STATES[2] +
    viverra_async_DW.Filter3_FILT_STATES[4]) + 0.0 *
    viverra_async_DW.Filter3_FILT_STATES[5];

  /* S-Function (sdspbiquad): '<Root>/Filter4' incorporates:
   *  Inport: '<Root>/In2'
   */
  viverra_async_DW.Filter4_TEMP_STATES[0] = (0.0076291496985537149 *
    viverra_async_U.Ib - -1.8648062707899464 *
    viverra_async_DW.Filter4_FILT_STATES[0]) - 0.8953228695841613 *
    viverra_async_DW.Filter4_FILT_STATES[1];
  viverra_async_DW.Filter4_TEMP_STATES[1] = (((2.0 *
    viverra_async_DW.Filter4_FILT_STATES[0] +
    viverra_async_DW.Filter4_TEMP_STATES[0]) +
    viverra_async_DW.Filter4_FILT_STATES[1]) * 0.0070335126208999361 -
    -1.7192136685456447 * viverra_async_DW.Filter4_FILT_STATES[2]) -
    0.74734771902924446 * viverra_async_DW.Filter4_FILT_STATES[3];
  viverra_async_DW.Filter4_TEMP_STATES[2] = (((2.0 *
    viverra_async_DW.Filter4_FILT_STATES[2] +
    viverra_async_DW.Filter4_TEMP_STATES[1]) +
    viverra_async_DW.Filter4_FILT_STATES[3]) * 0.082642823479589539 -
    -0.83471435304082109 * viverra_async_DW.Filter4_FILT_STATES[4]) - 0.0 *
    viverra_async_DW.Filter4_FILT_STATES[5];
  viverra_async_B.Filter4 = (viverra_async_DW.Filter4_TEMP_STATES[2] +
    viverra_async_DW.Filter4_FILT_STATES[4]) + 0.0 *
    viverra_async_DW.Filter4_FILT_STATES[5];

  /* Sum: '<Root>/Sum' */
  rtb_Iin_idx_2 = (0.0 - viverra_async_B.Filter3) - viverra_async_B.Filter4;

  /* Product: '<Root>/Product1' incorporates:
   *  Constant: '<Root>/ClarkeP'
   *  SignalConversion: '<Root>/ConcatBufferAtIinIn1'
   *  SignalConversion: '<Root>/ConcatBufferAtIinIn2'
   */
  for (i = 0; i < 3; i++) {
    rtb_Product1[i] = rtCP_ClarkeP_Value[i + 6] * rtb_Iin_idx_2 +
      (rtCP_ClarkeP_Value[i + 3] * viverra_async_B.Filter4 +
       rtCP_ClarkeP_Value[i] * viverra_async_B.Filter3);
  }

  /* End of Product: '<Root>/Product1' */

  /* Fcn: '<S2>/x->r' */
  rtb_Iin_idx_2 = rt_hypotd_snf(rtb_Product1[0], rtb_Product1[1]);

  /* Outport: '<Root>/Iabs' */
  viverra_async_Y.Iabs = rtb_Iin_idx_2;

  /* MATLAB Function: '<Root>/MATLAB Function' incorporates:
   *  Fcn: '<S2>/x->theta'
   *  Sum: '<Root>/Sum1'
   */
  /* MATLAB Function 'MATLAB Function': '<S4>:1' */
  /* '<S4>:1:3' tmp = u/(2*pi); */
  rtb_xtheta_f = (rt_atan2d_snf(rtb_Product1[1], rtb_Product1[0]) - rtb_xtheta_f)
    / 6.2831853071795862;

  /* '<S4>:1:4' tmp = (tmp-fix(tmp))*2*pi; */
  if (rtb_xtheta_f < 0.0) {
    rtb_xtheta_a = ceil(rtb_xtheta_f);
  } else {
    rtb_xtheta_a = floor(rtb_xtheta_f);
  }

  rtb_xtheta_f = (rtb_xtheta_f - rtb_xtheta_a) * 2.0 * 3.1415926535897931;

  /* '<S4>:1:5' if tmp > pi */
  if (rtb_xtheta_f > 3.1415926535897931) {
    /* Outport: '<Root>/phi' */
    /* '<S4>:1:6' y = tmp - 2*pi; */
    viverra_async_Y.phi = rtb_xtheta_f - 6.2831853071795862;
  } else if (rtb_xtheta_f < -3.1415926535897931) {
    /* Outport: '<Root>/phi' */
    /* '<S4>:1:7' elseif tmp < -pi */
    /* '<S4>:1:8' y = tmp + 2*pi; */
    viverra_async_Y.phi = rtb_xtheta_f + 6.2831853071795862;
  } else {
    /* Outport: '<Root>/phi' */
    /* '<S4>:1:9' else */
    /* '<S4>:1:10' y = tmp; */
    viverra_async_Y.phi = rtb_xtheta_f;
  }

  /* End of MATLAB Function: '<Root>/MATLAB Function' */

  /* MATLAB Function: '<Root>/MATLAB Function1' */
  /* MATLAB Function 'MATLAB Function1': '<S5>:1' */
  /* '<S5>:1:3' y = x1*x2+y1*y2; */
  rtb_xtheta_f = rtb_Product[0] * rtb_Product1[0] + rtb_Product[1] *
    rtb_Product1[1];

  /* Outport: '<Root>/Pact' */
  viverra_async_Y.Pact = rtb_xtheta_f;

  /* Outport: '<Root>/Pm' incorporates:
   *  Inport: '<Root>/Rs'
   *  Math: '<Root>/Math Function'
   *  Product: '<Root>/Product2'
   *  Sum: '<Root>/Sum2'
   */
  viverra_async_Y.Pm = rtb_xtheta_f - rtb_Iin_idx_2 * rtb_Iin_idx_2 *
    viverra_async_U.Rs;

  /* Outport: '<Root>/Add1' */
  viverra_async_Y.Add1 = rtb_Product[2];

  /* Update for S-Function (sdspbiquad): '<Root>/Filter' */
  viverra_async_DW.Filter_FILT_STATES[1] = viverra_async_DW.Filter_FILT_STATES[0];
  viverra_async_DW.Filter_FILT_STATES[0] = viverra_async_DW.Filter_TEMP_STATES[0];
  viverra_async_DW.Filter_FILT_STATES[3] = viverra_async_DW.Filter_FILT_STATES[2];
  viverra_async_DW.Filter_FILT_STATES[2] = viverra_async_DW.Filter_TEMP_STATES[1];
  viverra_async_DW.Filter_FILT_STATES[5] = viverra_async_DW.Filter_FILT_STATES[4];
  viverra_async_DW.Filter_FILT_STATES[4] = viverra_async_DW.Filter_TEMP_STATES[2];

  /* Update for S-Function (sdspbiquad): '<Root>/Filter1' */
  viverra_async_DW.Filter1_FILT_STATES[1] =
    viverra_async_DW.Filter1_FILT_STATES[0];
  viverra_async_DW.Filter1_FILT_STATES[0] =
    viverra_async_DW.Filter1_TEMP_STATES[0];
  viverra_async_DW.Filter1_FILT_STATES[3] =
    viverra_async_DW.Filter1_FILT_STATES[2];
  viverra_async_DW.Filter1_FILT_STATES[2] =
    viverra_async_DW.Filter1_TEMP_STATES[1];
  viverra_async_DW.Filter1_FILT_STATES[5] =
    viverra_async_DW.Filter1_FILT_STATES[4];
  viverra_async_DW.Filter1_FILT_STATES[4] =
    viverra_async_DW.Filter1_TEMP_STATES[2];

  /* Update for S-Function (sdspbiquad): '<Root>/Filter2' */
  viverra_async_DW.Filter2_FILT_STATES[1] =
    viverra_async_DW.Filter2_FILT_STATES[0];
  viverra_async_DW.Filter2_FILT_STATES[0] =
    viverra_async_DW.Filter2_TEMP_STATES[0];
  viverra_async_DW.Filter2_FILT_STATES[3] =
    viverra_async_DW.Filter2_FILT_STATES[2];
  viverra_async_DW.Filter2_FILT_STATES[2] =
    viverra_async_DW.Filter2_TEMP_STATES[1];
  viverra_async_DW.Filter2_FILT_STATES[5] =
    viverra_async_DW.Filter2_FILT_STATES[4];
  viverra_async_DW.Filter2_FILT_STATES[4] =
    viverra_async_DW.Filter2_TEMP_STATES[2];

  /* Update for S-Function (sdspbiquad): '<Root>/Filter3' */
  viverra_async_DW.Filter3_FILT_STATES[1] =
    viverra_async_DW.Filter3_FILT_STATES[0];
  viverra_async_DW.Filter3_FILT_STATES[0] =
    viverra_async_DW.Filter3_TEMP_STATES[0];
  viverra_async_DW.Filter3_FILT_STATES[3] =
    viverra_async_DW.Filter3_FILT_STATES[2];
  viverra_async_DW.Filter3_FILT_STATES[2] =
    viverra_async_DW.Filter3_TEMP_STATES[1];
  viverra_async_DW.Filter3_FILT_STATES[5] =
    viverra_async_DW.Filter3_FILT_STATES[4];
  viverra_async_DW.Filter3_FILT_STATES[4] =
    viverra_async_DW.Filter3_TEMP_STATES[2];

  /* Update for S-Function (sdspbiquad): '<Root>/Filter4' */
  viverra_async_DW.Filter4_FILT_STATES[1] =
    viverra_async_DW.Filter4_FILT_STATES[0];
  viverra_async_DW.Filter4_FILT_STATES[0] =
    viverra_async_DW.Filter4_TEMP_STATES[0];
  viverra_async_DW.Filter4_FILT_STATES[3] =
    viverra_async_DW.Filter4_FILT_STATES[2];
  viverra_async_DW.Filter4_FILT_STATES[2] =
    viverra_async_DW.Filter4_TEMP_STATES[1];
  viverra_async_DW.Filter4_FILT_STATES[5] =
    viverra_async_DW.Filter4_FILT_STATES[4];
  viverra_async_DW.Filter4_FILT_STATES[4] =
    viverra_async_DW.Filter4_TEMP_STATES[2];
}

/* Model initialize function */
void viverra_async_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize error status */
  rtmSetErrorStatus(viverra_async_M, (NULL));

  /* block I/O */
  (void) memset(((void *) &viverra_async_B), 0,
                sizeof(B_viverra_async_T));

  /* states (dwork) */
  (void) memset((void *)&viverra_async_DW, 0,
                sizeof(DW_viverra_async_T));

  /* external inputs */
  (void)memset((void *)&viverra_async_U, 0, sizeof(ExtU_viverra_async_T));

  /* external outputs */
  (void) memset((void *)&viverra_async_Y, 0,
                sizeof(ExtY_viverra_async_T));

  {
    int32_T i;
    for (i = 0; i < 6; i++) {
      /* InitializeConditions for S-Function (sdspbiquad): '<Root>/Filter' */
      viverra_async_DW.Filter_FILT_STATES[i] = 0.0;

      /* InitializeConditions for S-Function (sdspbiquad): '<Root>/Filter1' */
      viverra_async_DW.Filter1_FILT_STATES[i] = 0.0;

      /* InitializeConditions for S-Function (sdspbiquad): '<Root>/Filter2' */
      viverra_async_DW.Filter2_FILT_STATES[i] = 0.0;

      /* InitializeConditions for S-Function (sdspbiquad): '<Root>/Filter3' */
      viverra_async_DW.Filter3_FILT_STATES[i] = 0.0;

      /* InitializeConditions for S-Function (sdspbiquad): '<Root>/Filter4' */
      viverra_async_DW.Filter4_FILT_STATES[i] = 0.0;
    }
  }
}

/* Model terminate function */
void viverra_async_terminate(void)
{
  /* (no terminate code required) */
}
