/*
 * viverra_async_private.h
 *
 * Code generation for model "viverra_async".
 *
 * Model version              : 1.391
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Mon Jun 12 07:46:43 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_viverra_async_private_h_
#define RTW_HEADER_viverra_async_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"

extern const real_T rtCP_pooled_xn7JOoq8EbEY[9];

#define rtCP_ClarkeP_Value             rtCP_pooled_xn7JOoq8EbEY  /* Expression: sqrt(2/3)*[1 -1/2 -1/2; 0 sqrt(3)/2 -sqrt(3)/2; 1/sqrt(2) 1/sqrt(2) 1/sqrt(2)]
                                                                  * Referenced by: '<Root>/ClarkeP'
                                                                  */
#endif                                 /* RTW_HEADER_viverra_async_private_h_ */
