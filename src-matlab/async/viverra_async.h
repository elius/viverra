/*
 * viverra_async.h
 *
 * Code generation for model "viverra_async".
 *
 * Model version              : 1.391
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Mon Jun 12 07:46:43 2017
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Linux 64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_viverra_async_h_
#define RTW_HEADER_viverra_async_h_
#include <math.h>
#include <string.h>
#include <stddef.h>
#ifndef viverra_async_COMMON_INCLUDES_
# define viverra_async_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* viverra_async_COMMON_INCLUDES_ */

#include "viverra_async_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_atan2d_snf.h"
#include "rt_hypotd_snf.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals (auto storage) */
typedef struct {
  real_T Uin[3];                       /* '<Root>/Uin' */
  real_T Filter3;                      /* '<Root>/Filter3' */
  real_T Filter4;                      /* '<Root>/Filter4' */
} B_viverra_async_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Filter_FILT_STATES[6];        /* '<Root>/Filter' */
  real_T Filter1_FILT_STATES[6];       /* '<Root>/Filter1' */
  real_T Filter2_FILT_STATES[6];       /* '<Root>/Filter2' */
  real_T Filter3_FILT_STATES[6];       /* '<Root>/Filter3' */
  real_T Filter4_FILT_STATES[6];       /* '<Root>/Filter4' */
  real_T Filter_TEMP_STATES[3];        /* '<Root>/Filter' */
  real_T Filter1_TEMP_STATES[3];       /* '<Root>/Filter1' */
  real_T Filter2_TEMP_STATES[3];       /* '<Root>/Filter2' */
  real_T Filter3_TEMP_STATES[3];       /* '<Root>/Filter3' */
  real_T Filter4_TEMP_STATES[3];       /* '<Root>/Filter4' */
} DW_viverra_async_T;

/* External inputs (root inport signals with auto storage) */
typedef struct {
  real_T Ua;                           /* '<Root>/In3' */
  real_T Ub;                           /* '<Root>/In4' */
  real_T Uc;                           /* '<Root>/In5' */
  real_T Ia;                           /* '<Root>/Ia' */
  real_T Ib;                           /* '<Root>/In2' */
  real_T Rs;                           /* '<Root>/Rs' */
} ExtU_viverra_async_T;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  real_T Uabs;                         /* '<Root>/Uabs' */
  real_T Uang;                         /* '<Root>/Uang' */
  real_T Iabs;                         /* '<Root>/Iabs' */
  real_T phi;                          /* '<Root>/phi' */
  real_T Pact;                         /* '<Root>/Pact' */
  real_T Pm;                           /* '<Root>/Pm' */
  real_T Add1;                         /* '<Root>/Add1' */
} ExtY_viverra_async_T;

/* Real-time Model Data Structure */
struct tag_RTM_viverra_async_T {
  const char_T *errorStatus;
};

/* Block signals (auto storage) */
extern B_viverra_async_T viverra_async_B;

/* Block states (auto storage) */
extern DW_viverra_async_T viverra_async_DW;

/* External inputs (root inport signals with auto storage) */
extern ExtU_viverra_async_T viverra_async_U;

/* External outputs (root outports fed by signals with auto storage) */
extern ExtY_viverra_async_T viverra_async_Y;

/* Model entry point functions */
extern void viverra_async_initialize(void);
extern void viverra_async_step(void);
extern void viverra_async_terminate(void);

/* Real-time Model object */
extern RT_MODEL_viverra_async_T *const viverra_async_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'viverra_async'
 * '<S1>'   : 'viverra_async/Cartesian to Polar'
 * '<S2>'   : 'viverra_async/Cartesian to Polar1'
 * '<S3>'   : 'viverra_async/Emulate'
 * '<S4>'   : 'viverra_async/MATLAB Function'
 * '<S5>'   : 'viverra_async/MATLAB Function1'
 */
#endif                                 /* RTW_HEADER_viverra_async_h_ */
