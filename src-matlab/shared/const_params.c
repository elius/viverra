/*
 * const_params.c
 *
 * Code generation for model "viverra_async".
 *
 * Model version              : 1.384
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Fri Jun  9 20:13:00 2017
 */
#include "rtwtypes.h"

extern const real_T rtCP_pooled_xn7JOoq8EbEY[9];
const real_T rtCP_pooled_xn7JOoq8EbEY[9] = { 0.816496580927726, 0.0,
  0.57735026918962573, -0.408248290463863, 0.70710678118654746,
  0.57735026918962573, -0.408248290463863, -0.70710678118654746,
  0.57735026918962573 } ;
