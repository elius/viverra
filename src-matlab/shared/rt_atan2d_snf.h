/*
 * /home/arhi/dev/matlab/viverra/slprj/grt/_sharedutils/rt_atan2d_snf.h
 *
 * Code generation for model "viverra_sync".
 *
 * Model version              : 1.338
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Fri Jun  9 20:10:58 2017
 * Created for block: viverra_sync
 */

#ifndef SHARE_rt_atan2d_snf
#define SHARE_rt_atan2d_snf
#include "rtwtypes.h"

extern real_T rt_atan2d_snf(real_T u0, real_T u1);

#endif
