/*
 * rtGetNaN.h
 *
 * Code generation for model "viverra_sync".
 *
 * Model version              : 1.338
 * Simulink Coder version : 8.11 (R2016b) 25-Aug-2016
 * C source code generated on : Fri Jun  9 20:10:58 2017
 */

#ifndef RTW_HEADER_rtGetNaN_h_
#define RTW_HEADER_rtGetNaN_h_
#include <stddef.h>
#include "rtwtypes.h"
#include "rt_nonfinite.h"

extern real_T rtGetNaN(void);
extern real32_T rtGetNaNF(void);

#endif                                 /* RTW_HEADER_rtGetNaN_h_ */
