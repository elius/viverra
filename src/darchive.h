/*
 * darchive.h
 *
 *  Created on: May 16, 2017
 *      Author: arhi
 */

#ifndef DARCHIVE_H_
#define DARCHIVE_H_

#include <time.h>
#include <stdio.h>

#include "dprocess.h"

#define ARCH_VERSION_MAJOR 0
#define ARCH_VERSION_MINOR 1

typedef struct __internal_param_desc_t {
	char label[32];
	char description[128];
} param_desc_t;

typedef struct __internal_archive_t {
	int interval;
	time_t endTime;
	char fname[64];
	FILE *f;
	int params;
	param_desc_t *pdesc;
} archive_t;

/*
 *  Initialize archive
 *  return 0 on success
 */
int initArchive(archive_t *arch, const char baseDir[], int interval, int params,
		const param_desc_t pdesc[params]);

/*
 * Close archive structures
 */
extern void closeArchive();

typedef struct __internal_archiving_param_t {
	archive_t *archive;
	data_buffer_t *dataBuf;
	int ctrl;
	int status;
} archiving_param_t;

// Archiving function
extern void* archiveData(archiving_param_t *param);

#endif /* DARCHIVE_H_ */
