/*
 * dprocess.h
 *
 *  Created on: May 16, 2017
 *      Author: arhi
 */

#ifndef DPROCESS_H_
#define DPROCESS_H_

#include <stdint.h>
#include <pthread.h>

#include "pstream.h"

#define pdata_t float

// Internal processing pack
typedef struct __internal_data_sample_t {
	uint32_t sec;	// time stamp seconds
	uint32_t nsec;	// time stamp nanoseconds
	pdata_t data[];	// parameters data
} data_sample_t;

// Processed data buffer
typedef struct __internal_data_buffer_t {
	int params;		// parameters in sample
	int basePeriod; // predefined sampling base period
	int capacity;	// buffer capacity
	int loaded;		// loaded samples
	int first_index;		// index of first element
	pthread_mutex_t mutex;	// mutex for thread safe functionality
	pthread_cond_t cond;	// condition variable for sync
	data_sample_t *samples;	// ring buffer of samples
} data_buffer_t;

#define sample_size(buf) (sizeof(data_sample_t)+sizeof(pdata_t)*((buf))->params)

// Initialize data buffer parameters and allocate memory
extern int initSampleDataBuffer(data_buffer_t *buf, const int params,
		const int basePeriod, const int capacity);
// Free memory from data buffer
extern void freeSampleDataBuffer(data_buffer_t *buf);
// Push data to buffer
extern int pushSampleDataBuffer(data_buffer_t *buf, const data_sample_t *sample);
// Pop data from buffer
extern int popSampleDataBuffer(data_buffer_t *buf, data_sample_t *sample,
		const struct timespec *timeout);
// Wait half or more buffer load
extern int waitHalfSampleDataBuffer(data_buffer_t *buf, const struct timespec *timeout);


typedef struct __internal_dataprocess_param_t {
	data_buffer_t *dataBuf;
	sens_raw_buf_t *netBuf;
	int ctrl;
	int status;
} dataprocess_param_t;

// Data process function
extern void* dataProcess(dataprocess_param_t *param);

#endif /* DPROCESS_H_ */
