/*
 * pstream.h
 *
 *  Created on: May 14, 2017
 *      Author: arhi
 *
 * Bind to UDP socket and capture packets from low level capture device.
 * Captured packets checked for integrity and merged in data stream
 */

#ifndef PSTREAM_H_
#define PSTREAM_H_

#include <stdint.h>
#include <time.h>
#include <pthread.h>

#include "empcommon.h"

/*
 * Package structure
 *
 * | Offset          | Name     | Type     | Description                  |
 * | --------------- | -------- | -------- | ---------------------------- |
 * | 0               | magic    | 4\*char  | 'EMPP' start sequence        |
 * | 4               | period   | uint32_t | Data time period in 1e-6 sec |
 * | 8               | params   | uint16_t | Parameters count             |
 * | 10              | id       | uint16_t | Packet number with overflow  |
 * | 12              | counts   | uint16_t | Time ticks in packet         |
 * | 14+i\*2\*params | p1       | uint16_t | Param1 for tick i            |
 * | 16+i\*2\*params | p2       | uint16_t | Param2 for tick i            |
 */

typedef struct __internal_empp_pack_t {
	struct empp_pack_header {
		char magic[4];
		uint32_t period;
		uint16_t params;
		uint16_t id;
		uint16_t counts;
	}__attribute__ ((packed)) header;
	uint16_t data[];
} empp_pack_t;

// Max pack size is Ethernet MTU - 42 byte of Ethernet + IP + UDP header
#define EMPP_MAX_SIZE (1500-42)
#define EMPP_HEADER_SIZE sizeof(struct empp_pack_header)
#define EMPP_RAW_PACK_DATA_MAX (EMPP_MAX_SIZE - EMPP_HEADER_SIZE)

// Internal raw packet
typedef struct __internal_sens_raw_pack_t {
	uint32_t sec;		// time stamp seconds
	uint32_t nsec;		// time stamp nanoseconds
	uint16_t is_first;	// first pack in series
	uint16_t counts;	// data counts in pack
	uint16_t data[EMPP_RAW_PACK_DATA_MAX];	// pack data
} sens_raw_pack_t;

// Received packet buffer
typedef struct __internal_sens_raw_buf_t {
	int params;			// parameters in one sample
	int last_pack_id;	// last pushed pack ID
	int capacity;		// packets buffer size
	volatile int loaded;			// loaded elements
	int first_index;	// index of first loaded packet
	pthread_mutex_t mutex;	// mutex for thread safe functionality
	pthread_cond_t cond;	// condition for waiting data signaling
	sens_raw_pack_t *packs;	// ring buffer of packets
} sens_raw_buf_t;

// Initialize raw packet buffer parameters and allocate memory
extern int initRawDataBuffer(sens_raw_buf_t *buf, const int params, const int capacity);
// Free memory from raw packet buffer
extern void freeRawDataBuffer(sens_raw_buf_t *buf);
// Push packet to buffer
extern int pushRawDataBuffer(sens_raw_buf_t *buf, const empp_pack_t *pack, const struct timespec *ts);
// Pop data packet from buffer
extern int popRawDataBuffer(sens_raw_buf_t *buf, sens_raw_pack_t *pack, const struct timespec *timeout);

typedef struct __internal_netcatch_param_t {
	sens_raw_buf_t *dataBuf;
	uint16_t port;
	uint32_t saddress;
	int ctrl;
	int status;
} netcatch_param_t;

// Net stream catcher function
extern void* netCatch(netcatch_param_t *param);

#endif /* PSTREAM_H_ */
