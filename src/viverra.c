/*
 ============================================================================
 Name        : viverra.c
 Author      : arhi
 Version     :
 Copyright   : EliusM copyright
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "darchive.h"

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include <arpa/inet.h>
#include <signal.h>
#include <string.h>

#include "pstream.h"
#include "dprocess.h"

#include "config.h"

#define NET_PARAMS 5
#define NET_BUF_CAPACITY 100

#define PROCESSED_PARAMS 12

#define PROCESSED_BUF_CAPACITY 10000
#define BASE_PERIOD 1000

// Signal captured flag
volatile sig_atomic_t sig_term = 0;
void term_signal(int signal_number) {
	sig_term = 1;
}

/**
 * Main program function
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char* argv[]) {
	//---------------
	// Configure sigaction
	struct sigaction sa;
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = &term_signal;
	if (sigaction(SIGTERM, &sa, NULL) != 0) {
		puts("Failed to register SIGTERM handler!");
		return EXIT_FAILURE;
	}
	if (sigaction(SIGINT, &sa, NULL) != 0) {
		puts("Failed to register SIGINT handler!");
		return EXIT_FAILURE;
	}

	//---------------
	// Parse command line arguments
	argp_parse(&argp, argc, argv, 0, NULL, NULL);

	//---------------
	// Read configuration file
	if (working_params.common.conf_name != NULL) {
		if (read_config(working_params.common.conf_name) != EXIT_SUCCESS) {
			fprintf(stderr, "Configuration failure!\n");
			return EXIT_FAILURE;
		}
	}

	if (working_params.common.verbose)
		print_params();

	if (working_params.common.test)
		return EXIT_SUCCESS;

	//---------------
	// Network stream

	sens_raw_buf_t netDataBuf;

	if (initRawDataBuffer(&netDataBuf, NET_PARAMS, NET_BUF_CAPACITY)) {
		puts("initRawDataBuffer error");
		return EXIT_FAILURE;
	}

	pthread_t idNetCatch = 0;
	netcatch_param_t netCatchParams = { .dataBuf = &netDataBuf, .port =
			working_params.net.port,
			.saddress = INADDR_ANY, .ctrl = 0 };

	if (pthread_create(&idNetCatch, NULL, (void * (*)(void *)) netCatch,
			&netCatchParams)) {
		puts("Failed to create net catcher");
		return EXIT_FAILURE;
	}

	//---------------
	// Processing stream

	data_buffer_t processedDataBuf;

	if (initSampleDataBuffer(&processedDataBuf, PROCESSED_PARAMS, BASE_PERIOD,
	PROCESSED_BUF_CAPACITY)) {
		puts("initSampleDataBuffer error");
		return EXIT_FAILURE;
	}
	pthread_t idProcess = 0;
	dataprocess_param_t dataProcessParams = {
			.dataBuf = &processedDataBuf,
			.netBuf = &netDataBuf,
			.ctrl = 0,
			.status = 0
	};

	if (pthread_create(&idProcess, NULL, (void* (*)(void*)) dataProcess,
			&dataProcessParams)) {
		puts("Failed to create process thread");
		return EXIT_FAILURE;
	}

	//---------------
	// Archiving stream

	archive_t arch;
	param_desc_t pdesc[PROCESSED_PARAMS] = {
			[0].label = "Ua", [0].description = "U phase A",
			[1].label = "Ub", [1].description = "U phase B",
			[2].label = "Uc", [2].description = "U phase C",
			[3].label = "Ia", [3].description = "I phase A",
			[4].label = "Ib", [4].description = "I phase B",
			[5].label = "Uabs", [5].description = "U vector absolute",
			[6].label = "Uang", [6].description = "U vector angle",
			[7].label = "Iabs", [7].description = "I vector absolute",
			[8].label = "Phi", [8].description = "U/I delay angle",
			[9].label = "Pact", [9].description = "P active",
			[10].label = "Pm", [10].description = "P mechanical",
			[11].label = "Ug", [11].description = "U vector error"
	};

	if (initArchive(&arch, working_params.arch.dir,
			working_params.arch.interval,
			PROCESSED_PARAMS, pdesc)) {
		puts("Failed to initialize archiving");
		return EXIT_FAILURE;
	}
	pthread_t idArchive = 0;
	archiving_param_t archiveProcessParams = { .archive = &arch, .dataBuf =
			&processedDataBuf, .ctrl = 0 };

	if (pthread_create(&idArchive, NULL, (void* (*)(void*)) archiveData,
			&archiveProcessParams)) {
		puts("Failed to create archiving thread");
		return EXIT_FAILURE;
	}

	struct timespec timeout = { .tv_sec = 0, .tv_nsec = 200000000 };
	while (!sig_term) {
		int i = fgetc(stdin);
		if (i > 0) {
			if (i == 'q' || i == 'Q')
				break;
			continue;
		}
		nanosleep(&timeout, NULL);
	}

	//---------------
	// Sequential stop working threads
	puts("Stopping network...");
	netCatchParams.ctrl = 1;
	pthread_join(idNetCatch, NULL);
	puts("Stopping processing...");
	dataProcessParams.ctrl = 1;
	pthread_join(idProcess, NULL);
	puts("Stopping archiving...");
	archiveProcessParams.ctrl = 1;
	pthread_join(idArchive, NULL);
	puts("Stopped.");

	//---------------
	// Free resources
	freeRawDataBuffer(&netDataBuf);
	freeSampleDataBuffer(&processedDataBuf);
	closeArchive(&arch);

	return EXIT_SUCCESS;
}
