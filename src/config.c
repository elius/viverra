/*
 * config.c
 *
 *  Created on: Jun 4, 2017
 *      Author: arhi
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

work_params_t working_params = {
		.common = {
				.verbose = 0,
				.test = 0,
				.conf_name = NULL
		},
		.net = {
				.host = "localhost",
				.port = 1024
		},
		.arch = {
				.dir = "/mnt/arch",
				.interval = 2
		},
		.stream = {
				.ch_scale = { 1.0, 1.0, 1.0, 1.0, 1.0 },
				.ch_shift = { 2048.0, 2048.0, 2048.0, 2048.0, 2048.0 },
				.u_ch = { 1, 2, 3 },
				.i_ch = { 4, 5 }
		},
		.processing = {
				.r_stator = 0.01,
				.averaging = 10,
				.min_u = 110.0
		}
};

/**
 * Print working parameters
 */
void print_params() {
	printf("- Common\n");
	printf("\tverbose: %s\n",
			working_params.common.verbose != 0 ? "yes" : "no");
	printf("\ttest: %s\n", working_params.common.test != 0 ? "yes" : "no");
	printf("\tconfiguration: %s\n", working_params.common.conf_name);

	printf("- Network\n");
	printf("\taddress: %s\n", working_params.net.host);
	printf("\tport: %d\n", working_params.net.port);

	printf("- Archiving\n");
	printf("\tdirectory: %s\n", working_params.arch.dir);
	printf("\tinterval: %d\n", working_params.arch.interval);

	printf("- Stream\n");
	for (int i = 0; i < 5; i++) {
		printf("\tchannel %d:\n", i + 1);
		printf("\t\tscale: %f\n", working_params.stream.ch_scale[i]);
		printf("\t\tshift: %f\n", working_params.stream.ch_shift[i]);
	}
	printf("\tvoltage channels: %d, %d, %d\n", working_params.stream.u_ch[0],
			working_params.stream.u_ch[1], working_params.stream.u_ch[2]);
	printf("\tcurrent channels: %d, %d\n", working_params.stream.i_ch[0],
				working_params.stream.i_ch[1]);

	printf("- Processing\n");
	printf("\tstator resistance: %f\n", working_params.processing.r_stator);
	printf("\taveraging: %d\n", working_params.processing.averaging);
	printf("\tminimum voltage: %f\n", working_params.processing.min_u);
}

/*
 * ----------------------------
 * Command line parameters
 * ----------------------------
 */

#define STRINGIZER(arg)	#arg
#define STR_VALUE(arg) STRINGIZER(arg)

const char *argp_program_version =
#ifdef MATLAB_SYNC
		"viverra "STR_VALUE(VERSION_MAJOR)"."STR_VALUE(VERSION_MINOR)", sync, build "__DATE__" "__TIME__;
#else
		"viverra "STR_VALUE(VERSION_MAJOR)"."STR_VALUE(VERSION_MINOR)", async, build "__DATE__" "__TIME__;
#endif
const char *argp_program_bug_address = "<kovalenko@elius.com.ua>";

// Program documentation string.
char doc[] = "Catcher and first stage calculator for Empala data stream";
// A description of the arguments we accept.
char args_doc[] = "";
// The options we understand.
struct argp_option options[] = {
		{ "verbose", 'v', 0, 0, "produce verbose output" },
		{ "nono", 'n', 0, 0,
				"Just parse parameters and configuration file and exit" },
		{ "config", 'c', "FILE", 0, "Read configuration from FILE" },
		{ "directory", 'd', "DIRECTORY", 0, "write output file to DIRECTORY" },
		{ "minutes", 'm', "MINUTES", 0,
				"time capacity of one archive file in MUNITES" },
		{ "average", 'a', "INTERVAL", 0, "averaging INTERVAL in counts" },
		{ 0 } };

/**
 * Parse a single option.
 * @param key
 * @param arg
 * @param state
 * @return
 */
error_t parse_opt(int key, char *arg, struct argp_state *state) {
	int i_tmp = 0;

	switch (key) {
	case 'v':
		working_params.common.verbose = 1;
		break;
	case 'n':
		working_params.common.test = 1;
		break;
	case 'c':
		working_params.common.conf_name = arg;
		break;
	case 'd':
		working_params.arch.dir = arg;
		break;
	case 'm':
		i_tmp = strtol(arg, NULL, 0);
		if (i_tmp > 0)
			working_params.arch.interval = i_tmp;
		break;
	case 'a':
		i_tmp = strtol(arg, NULL, 0);
		if (i_tmp > 0)
			working_params.processing.averaging = i_tmp;
		break;
	case ARGP_KEY_NO_ARGS:
		break;
		/*argp_usage(state);
		 break;*/
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

// Main argp parameters
struct argp argp =
		{ .options = options, .parser = parse_opt, .args_doc = args_doc, .doc =
				doc, .children = 0, .help_filter = 0, .argp_domain = 0 };

/*
 * ----------------------------
 * Configuration file
 * ----------------------------
 */

config_t cfg;

/**
 * Read phase order from string "ABC"
 * @param size count of phases 2 or 3
 * @param str null terminated string with order
 * @param order	array of order indexes from 1 to size
 * @return negative value if error occurred, 0 on success
 */
int read_order(unsigned int size, const char *str, int order[size]) {
	if (size < 2 || size > 3)
		return -1;
	if (strlen(str) != size)
		return -1;
	int res[size];
	for (int i = 0; i < size; i++)
		res[i] = -1;
	for (int i = 0; i < size; i++) {
		int code = str[i];
		if (code >= 'a')
			code = code - 'a' + 'A';
		code -= 'A';
		if (code < 0 || code >= size || res[i] >= 0)
			return -2;
		res[i] = code + 1;
	}
	for (int i = 0; i < 3; i++)
		order[i] = res[i];
	return 0;
}

/**
 * Read configuration from file and fill working parameters
 * @param fname null terminated string with config file name
 * @return EXIT_SUCCESS if OK, EXIT_FAILURE - otherwise
 */
int read_config(const char *fname) {
	// try to open file
	config_setting_t *setting;

	config_init(&cfg);

	// Read the file. If there is an error, report it and exit.
	if (!config_read_file(&cfg, fname)) {
		fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
				config_error_line(&cfg), config_error_text(&cfg));
		config_destroy(&cfg);
		return EXIT_FAILURE;
	}

	// "archive" section
	setting = config_lookup(&cfg, "archive");
	if (setting != NULL) {
		const char *param_str;
		if (config_setting_lookup_string(setting, "directory",
				&param_str) == CONFIG_TRUE)
			working_params.arch.dir = param_str;
		int param_i;
		if (config_setting_lookup_int(setting, "interval",
				&param_i) == CONFIG_TRUE)
			working_params.arch.interval = (uint16_t) param_i;
	}

	// "processing" section
	setting = config_lookup(&cfg, "processing");
	if (setting != NULL) {
		double param_d;
		if (config_setting_lookup_float(setting, "min_u",
				&param_d) == CONFIG_TRUE)
			working_params.processing.min_u = param_d;
		int param_i;
		if (config_setting_lookup_int(setting, "averaging",
				&param_i) == CONFIG_TRUE)
			working_params.processing.averaging = param_i;

	}

	// "stream" section
	setting = config_lookup(&cfg, "stream");
	if (setting != NULL) {
		config_setting_t *setting_chs = config_setting_lookup(setting,
				"channels");
		if (setting_chs != NULL) {
			for (int i = 0; i < 5; i++) {
				char ch_name[4] = "chX";
				ch_name[2] = '1' + i;
				config_setting_t *setting_ch = config_setting_lookup(
						setting_chs,
						ch_name);
				if (setting_ch != NULL) {
					double param_d;
					if (config_setting_lookup_float(setting_ch, "scale",
							&param_d) == CONFIG_TRUE)
						working_params.stream.ch_scale[i] = param_d;
					if (config_setting_lookup_float(setting_ch, "shift",
							&param_d) == CONFIG_TRUE)
						working_params.stream.ch_shift[i] = param_d;
				}
			}
		}

		config_setting_t *settingv = config_setting_lookup(setting,
				"voltage");
		if (settingv != NULL) {
			config_setting_t *setting_ch = config_setting_get_member(settingv,
					"ch");
			if (setting_ch != NULL) {
				for (int i = 0; i < 3; i++) {
					int param_i = config_setting_get_int_elem(setting_ch, i);
					working_params.stream.u_ch[i] = param_i;
				}
			}
		}
		config_setting_t *settingc = config_setting_lookup(setting,
				"current");
		if (settingc != NULL) {
			config_setting_t *setting_ch = config_setting_get_member(settingc,
					"ch");
			if (setting_ch != NULL) {
				for (int i = 0; i < 2; i++) {
					int param_i = config_setting_get_int_elem(setting_ch, i);
					working_params.stream.i_ch[i] = param_i;
				}
			}
		}
	}
	return EXIT_SUCCESS;
}

void close_config() {
	config_destroy(&cfg);
}
