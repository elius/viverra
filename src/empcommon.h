/*
 * empcommon.h
 *
 *  Created on: May 18, 2017
 *      Author: arhi
 */

#ifndef EMPCOMMON_H_
#define EMPCOMMON_H_

#include <time.h>
#include <pthread.h>

// Base clock for timeouts
#define TIMEOUT_CLOCK CLOCK_MONOTONIC

/*
 * Sum of timespec structures with correct ts_nsec value in result
 * x = x + y
 */
extern void timespec_sum(struct timespec *x, const struct timespec *y);

/*
 * Same fuctionality as pthread_cond_timedwait but time is relative from call time
 * return result from pthread_cond_timedwait
 */
extern int cond_timedwait_rel(pthread_cond_t *cond, pthread_mutex_t *mutex,
		const struct timespec *reltime);

#endif /* EMPCOMMON_H_ */
