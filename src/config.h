/*
 * config.h
 *
 *  Created on: Jun 4, 2017
 *      Author: arhi
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <stdint.h>
#include <argp.h>
#include <libconfig.h>

#define VERSION_MAJOR 0
#define VERSION_MINOR 3

/**
 * Working parameters structure
 */
typedef struct __work_params_t {
	struct {
		int verbose;	// verbose output
		int test;		// test configuration
		const char *conf_name;	// configuration file name
	} common;
	struct {
		const char *host;	// bind address
		uint16_t port;		// listen port
	} net;	// Network parameters
	struct {
		const char *dir;	// archiving directory
		int interval;		// archiving file capacity interval
	} arch;	// Archiving parameters
	struct {
		float ch_scale[5], ch_shift[5]; // scaling parameters from discrete to unit, value = (disc-shift)*scale
		uint8_t u_ch[3]; // input channel indexes {1..5}
		uint8_t i_ch[2]; // input channel indexes {1..5}
	} stream;	// Input data stream parameters
	struct {
		float r_stator;	// stator resistance
		int averaging;	// averaging interval in ticks
		float min_u;	// minimum U metric for model processing
	} processing;	// Data processing parameters
} work_params_t;
// Main parameters variable
extern work_params_t working_params;

/**
 * Print working parameters
 */
extern void print_params();

/**
 * Argp variables
 */
// Program documentation.
extern char doc[];
// A description of the arguments we accept.
extern char args_doc[];
// The options we understand.
extern struct argp_option options[];
// Argp parameters
extern struct argp argp;

/**
 * Argp command line parameter parser
 * @param key
 * @param arg
 * @param state
 * @return
 */
extern error_t parse_opt(int key, char *arg, struct argp_state *state);

/**
 * Read configuration from file
 * @param fname pointer to constant null-terminated string with file name
 * @return EXIT_SUCCESS if OK
 */
extern int read_config(const char *fname);

#endif /* CONFIG_H_ */
