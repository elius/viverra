/*
 * dprocess.c
 *
 *  Created on: May 16, 2017
 *      Author: arhi
 */

#include "dprocess.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include "config.h"

#ifdef MATLAB_SYNC

#include "viverra_sync.h"
#define VIVERRA_INIT viverra_sync_initialize()
#define VIVERRA_STEP viverra_sync_step()
#define VIVERRA_TERMINATE viverra_sync_terminate()

#else // #ifdef MATLAB_SYNC

#include "viverra_async.h"
#define VIVERRA_INIT viverra_async_initialize()
#define VIVERRA_STEP viverra_async_step()
#define VIVERRA_TERMINATE viverra_async_terminate()

#endif // #ifdef MATLAB_SYNC

/*
 * Initialize data buffer and allocate memory
 * return 0 if success
 */
int initSampleDataBuffer(data_buffer_t *buf, const int params,
		const int basePeriod, const int capacity) {
	// check parameters
	if (!buf)
		return -1;
	if (basePeriod <= 0)
		return -2;
	if (capacity <= 0)
		return -3;
	if (params <= 0)
		return -4;

	// allocate memory
	buf->samples = (data_sample_t *) calloc(capacity,
			sizeof(data_sample_t) + sizeof(pdata_t) * params);

	if (buf->samples == NULL) {
		return -5;
	}

	// configure buffer parameters
	buf->params = params;
	buf->capacity = capacity;
	buf->first_index = 0;
	buf->loaded = 0;

	pthread_mutex_init(&(buf->mutex), NULL);
	pthread_condattr_t condattr;
	pthread_condattr_init(&condattr);
	pthread_condattr_setclock(&condattr, TIMEOUT_CLOCK);
	pthread_cond_init(&(buf->cond), &condattr);
	pthread_condattr_destroy(&condattr);

	return 0;
}

/*
 * Free memory from data buffer and clean structure
 * return 0 if success
 */
void freeSampleDataBuffer(data_buffer_t *buf) {
	if (!buf)
		return;

	if (buf->samples)
		free(buf->samples);

	buf->params = 0;
	buf->basePeriod = 0;
	buf->capacity = 0;
	buf->loaded = 0;
	buf->first_index = 0;
	buf->samples = NULL;

	pthread_mutex_destroy(&(buf->mutex));
}

/*
 * Push data to processed data buffer
 * return 0 if success
 */
int pushSampleDataBuffer(data_buffer_t *buf, const data_sample_t *sample) {
	if (!buf || !sample)
		return -1;

	// check for free space
	if (buf->loaded >= buf->capacity)
		return -2;

	// begin critical section
	if (pthread_mutex_lock(&(buf->mutex)))
		return -5;

	// Get current push index position
	int pushIndex = buf->first_index + buf->loaded;
	if (pushIndex >= buf->capacity)
		pushIndex -= buf->capacity;

	const int sampleSize = sample_size(buf);
	data_sample_t *pushPtr = (data_sample_t*) ((uint8_t*) buf->samples
			+ sampleSize * pushIndex);

	memcpy(pushPtr, sample, sampleSize);
	buf->loaded++;

	pthread_cond_signal(&(buf->cond));
	pthread_mutex_unlock(&(buf->mutex));
	// end critical section

	return 0;
}

/*
 * Pop data from samples buffer
 * return 0 if success
 */
int popSampleDataBuffer(data_buffer_t *buf, data_sample_t *sample,
		const struct timespec *timeout) {
	if (!buf || !sample)
		return -1;

	// begin critical section
	if (pthread_mutex_lock(&(buf->mutex)))
		return -3;

	// Potential problem present
	// pthread_cond_timedwait must be called in loop while(buf->loaded == 0)
	// But for simplicity and no critical problem if timeout not reached this realization allowed
	if (buf->loaded == 0)
		cond_timedwait_rel(&(buf->cond), &(buf->mutex), timeout);

	// If no elements for pop
	if (buf->loaded <= 0) {
		pthread_mutex_unlock(&(buf->mutex));
		return -2;
	}

	const int sampleSize = sample_size(buf);
	data_sample_t *popPtr = (data_sample_t*) ((uint8_t*) buf->samples
			+ sampleSize * buf->first_index);

	memcpy(sample, popPtr, sampleSize);

	buf->first_index += 1;
	if (buf->first_index >= buf->capacity)
		buf->first_index = 0;
	buf->loaded -= 1;

	pthread_mutex_unlock(&(buf->mutex));

	return 0;
	// end critical section
}

/*
 * Wait half or more buffer load in timeout period
 * return buffer load
 */
int waitHalfSampleDataBuffer(data_buffer_t *buf, const struct timespec *timeout) {
	if (buf == NULL)
		return -1;
	if (timeout == NULL)
		return buf->loaded;
	struct timespec endTime;
	clock_gettime(TIMEOUT_CLOCK, &endTime);
	timespec_sum(&endTime, timeout);
	// wait condition signal
	if (pthread_mutex_lock(&(buf->mutex)))
		return 0;
	while (buf->loaded * 2 < buf->capacity)
		if (pthread_cond_timedwait(&(buf->cond), &(buf->mutex),
				&endTime) == ETIMEDOUT)
			break;
	pthread_mutex_unlock(&(buf->mutex));
	return buf->loaded;
}

void* dataProcess(dataprocess_param_t *param) {
	if (!param)
		return NULL;
	if (!param->dataBuf || !param->netBuf) {
		param->status = -1;
		return NULL;
	}
	{
		// Check U and I orders and I sign
		uint8_t uph[3] = { 0, 0, 0 };
		for (int i = 0; i < 3; i++) {
			if (working_params.stream.u_ch[i] < 1
					|| working_params.stream.u_ch[i] > 5) {
				param->status = -1;
				return NULL;
			}
			uph[i] += 1;
		}
		if (uph[0] != 1 || uph[1] != 1 || uph[2] != 1) {
			param->status = -1;
			return NULL;
		}
		uint8_t iph[2] = { 0, 0 };
		for (int i = 0; i < 2; i++) {
			if (working_params.stream.i_ch[i] < 1
					|| working_params.stream.i_ch[i] > 5) {
				param->status = -1;
				return NULL;
			}
			iph[i] += 1;
		}
		if (iph[0] != 1 || iph[1] != 1) {
			param->status = -1;
			return NULL;
		}
	}

	VIVERRA_INIT;

	const int net_params = param->netBuf->params;

	uint8_t samplePH[sample_size(param->dataBuf)];
	data_sample_t *sample = (data_sample_t*) samplePH;

	uint8_t sample_avgPH[sample_size(param->dataBuf)];
	data_sample_t *sample_avg = (data_sample_t*) sample_avgPH;

	int sample_i = 0;
	const int proc_params = param->dataBuf->params;

	const pdata_t rescale_k[5] = {
			working_params.stream.ch_scale[0],
			working_params.stream.ch_scale[1],
			working_params.stream.ch_scale[2],
			working_params.stream.ch_scale[3],
			working_params.stream.ch_scale[4]
	};
	const pdata_t rescale_sh[5] = {
			working_params.stream.ch_shift[0],
			working_params.stream.ch_shift[1],
			working_params.stream.ch_shift[2],
			working_params.stream.ch_shift[3],
			working_params.stream.ch_shift[4]
	};
	const pdata_t min_u_metric = working_params.processing.min_u
			* working_params.processing.min_u * 1.5;
	const int avg_period = working_params.processing.averaging;

	while (param->ctrl == 0) {
		sens_raw_pack_t pack;
		struct timespec timeout = { .tv_sec = 1, .tv_nsec = 0 };
		if (popRawDataBuffer(param->netBuf, &pack, &timeout)) {
			continue;
		}

		// Process data from pack
		for (int i = 0; i < pack.counts; i++) {
			uint16_t *rsample = pack.data + i * net_params;
			pdata_t net_rescaled[net_params];
			memset(samplePH, 0, sizeof(samplePH));

			for (int j = 0; j < net_params; j++)
				net_rescaled[j] = ((pdata_t) rsample[j] - rescale_sh[j])
						* rescale_k[j];

			sample->sec = pack.sec;
			sample->nsec = pack.nsec;

			// model inputs
			pdata_t u_metric = 0.0;
#ifdef MATLAB_SYNC
			viverra_sync_U.Ua = net_rescaled[working_params.stream.u_ch[0] - 1];
			viverra_sync_U.Ub = net_rescaled[working_params.stream.u_ch[1] - 1];
			viverra_sync_U.Uc = net_rescaled[working_params.stream.u_ch[2] - 1];
			viverra_sync_U.Ia = net_rescaled[working_params.stream.i_ch[0] - 1];
			viverra_sync_U.Ib = net_rescaled[working_params.stream.i_ch[1] - 1];
			viverra_sync_U.Rs = working_params.processing.r_stator;
			u_metric = viverra_sync_U.Ua * viverra_sync_U.Ua
					+ viverra_sync_U.Ub * viverra_sync_U.Ub
					+ viverra_sync_U.Uc * viverra_sync_U.Uc;
#else
			viverra_async_U.Ua =
					net_rescaled[working_params.stream.u_ch[0] - 1];
			viverra_async_U.Ub =
					net_rescaled[working_params.stream.u_ch[1] - 1];
			viverra_async_U.Uc =
					net_rescaled[working_params.stream.u_ch[2] - 1];
			viverra_async_U.Ia =
					net_rescaled[working_params.stream.i_ch[0] - 1];
			viverra_async_U.Ib =
					net_rescaled[working_params.stream.i_ch[1] - 1];
			viverra_async_U.Rs = working_params.processing.r_stator;
			u_metric = viverra_async_U.Ua * viverra_async_U.Ua
					+ viverra_async_U.Ub * viverra_async_U.Ub
					+ viverra_async_U.Uc * viverra_async_U.Uc;
#endif
			// model step
			if (u_metric >= min_u_metric) {
				VIVERRA_STEP;
				// model outputs
				// Raw data
#ifdef MATLAB_SYNC
				sample->data[0] = (pdata_t) viverra_sync_U.Ua;
				sample->data[1] = (pdata_t) viverra_sync_U.Ub;
				sample->data[2] = (pdata_t) viverra_sync_U.Uc;
				sample->data[3] = (pdata_t) viverra_sync_U.Ia;
				sample->data[4] = (pdata_t) viverra_sync_U.Ib;
				sample->data[5] = (pdata_t) viverra_sync_Y.Uabs;
				sample->data[6] = (pdata_t) viverra_sync_Y.Uang;
				sample->data[7] = (pdata_t) viverra_sync_Y.Iabs;
				sample->data[8] = (pdata_t) viverra_sync_Y.phi;
				sample->data[9] = (pdata_t) viverra_sync_Y.Pact;
				sample->data[10] = (pdata_t) viverra_sync_Y.Pm;
				sample->data[11] = (pdata_t) viverra_sync_Y.Add1;
#else
				sample->data[0] = (pdata_t) viverra_async_U.Ua;
				sample->data[1] = (pdata_t) viverra_async_U.Ub;
				sample->data[2] = (pdata_t) viverra_async_U.Uc;
				sample->data[3] = (pdata_t) viverra_async_U.Ia;
				sample->data[4] = (pdata_t) viverra_async_U.Ib;
				sample->data[5] = (pdata_t) viverra_async_Y.Uabs;
				sample->data[6] = (pdata_t) viverra_async_Y.Uang;
				sample->data[7] = (pdata_t) viverra_async_Y.Iabs;
				sample->data[8] = (pdata_t) viverra_async_Y.phi;
				sample->data[9] = (pdata_t) viverra_async_Y.Pact;
				sample->data[10] = (pdata_t) viverra_async_Y.Pm;
				sample->data[11] = (pdata_t) viverra_async_Y.Add1;
#endif
				// averaging
				for (int j = 0; j < proc_params; j++)
					sample_avg->data[j] += sample->data[j];
				sample_avg->sec = sample->sec;
				sample_avg->nsec = sample->nsec;
				sample_i++;
			} else {
				for (int j = 0; j < proc_params; j++)
					sample_avg->data[j] = 0.0;
				sample_i = 0;
			}

			if (sample_i >= avg_period) {
				for (int j = 0; j < proc_params; j++)
					sample_avg->data[j] /= (pdata_t) sample_i;
				if (pushSampleDataBuffer(param->dataBuf, sample_avg))
					puts("Failed to put processed data");
				sample_i = 0;
				for (int j = 0; j < proc_params; j++)
					sample_avg->data[j] = 0.0;
			}
		}
	}

	puts("Exit from data processing");

	VIVERRA_TERMINATE;

	param->status = 0;
	return NULL;
}
