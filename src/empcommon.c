/*
 * empcommon.c
 *
 *  Created on: May 18, 2017
 *      Author: arhi
 */

#include "empcommon.h"

#include <time.h>
#include <pthread.h>
#include <errno.h>

/*
 * Sum of timespec structures with correct ts_nsec value in result
 * x = x + y
 */
void timespec_sum(struct timespec *x, const struct timespec *y) {
	x->tv_nsec += y->tv_nsec;
	x->tv_sec += y->tv_sec;
	if (x->tv_nsec >= 1000000000) {
		x->tv_nsec -= 1000000000;
		x->tv_sec += 1;
	}
}

/*
 * Same fuctionality as pthread_cond_timedwait but time is relative from call time
 * return result from pthread_cond_timedwait
 */
int cond_timedwait_rel(pthread_cond_t *cond, pthread_mutex_t *mutex,
		const struct timespec *reltime) {
	if (reltime != NULL) {
		struct timespec endTime;
		clock_gettime(TIMEOUT_CLOCK, &endTime);
		timespec_sum(&endTime, reltime);
		// wait condition signal
		return pthread_cond_timedwait(cond, mutex, &endTime);
	}
	return ETIMEDOUT;
}
