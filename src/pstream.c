/*
 * pstream.c
 *
 *  Created on: May 14, 2017
 *      Author: arhi
 */

#include "pstream.h"

#include <linux/sockios.h>

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <arpa/inet.h>

#include <unistd.h>
#include <errno.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
 * Initialize packet buffer and allocate memory
 * return 0 if success
 */
int initRawDataBuffer(sens_raw_buf_t *buf, const int params, const int capacity) {
	// check buffer
	if (!buf)
		return -1;

	// check size
	if (capacity <= 0)
		return -3;

	// check parameters
	if (params <= 0)
		return -4;

	// allocate memory
	buf->packs = (sens_raw_pack_t *) calloc(capacity, sizeof(sens_raw_pack_t));

	if (buf->packs == NULL) {
		return -5;
	}

	// configure buffer parameters
	buf->params = params;
	buf->capacity = capacity;
	buf->first_index = 0;
	buf->loaded = 0;
	buf->last_pack_id = -1;

	pthread_mutex_init(&(buf->mutex), NULL);
	pthread_condattr_t condattr;
	pthread_condattr_init(&condattr);
	pthread_condattr_setclock(&condattr, TIMEOUT_CLOCK);
	pthread_cond_init(&(buf->cond), &condattr);
	pthread_condattr_destroy(&condattr);

	return 0;
}

/*
 * Free memory from data buffer and clean structure
 * return 0 if success
 */
void freeRawDataBuffer(sens_raw_buf_t *buf) {
	if (!buf)
		return;

	if (buf->packs)
		free(buf->packs);

	buf->packs = NULL;
	buf->params = 0;
	buf->capacity = 0;
	buf->first_index = 0;
	buf->loaded = 0;
	buf->last_pack_id = -1;

	pthread_mutex_destroy(&(buf->mutex));
	pthread_cond_destroy(&(buf->cond));
}

/*
 * Push data from network packet to internal buffer
 * return 0 if success
 */
int pushRawDataBuffer(sens_raw_buf_t *buf, const empp_pack_t *pack,
		const struct timespec *ts) {
	if (!buf || !pack || !ts)
		return -1;
	// check for free space
	if (buf->loaded >= buf->capacity)
		return -2;

	// check for correct parameters count
	if (pack->header.params != buf->params)
		return -3;

	// check data section size
	int dataSize = pack->header.counts * pack->header.params * sizeof(uint16_t);
	if (dataSize > EMPP_RAW_PACK_DATA_MAX)
		return -4;

	// begin critical section
	if (pthread_mutex_lock(&(buf->mutex)))
		return -5;

	// Get current push index position
	int pushIndex = buf->first_index + buf->loaded;
	if (pushIndex >= buf->capacity)
		pushIndex -= buf->capacity;
	sens_raw_pack_t *pushPtr = buf->packs + pushIndex;

	// Copy data to new place
	pushPtr->counts = pack->header.counts;
	pushPtr->sec = (uint32_t) ts->tv_sec;
	pushPtr->nsec = (uint32_t) ts->tv_nsec;
	memcpy(pushPtr->data, pack->data, dataSize);
	pushPtr->is_first = buf->last_pack_id < 0
			|| ((uint16_t) (pack->header.id - buf->last_pack_id) != 1u);
	buf->last_pack_id = pack->header.id;
	buf->loaded++;

	pthread_cond_signal(&(buf->cond));
	pthread_mutex_unlock(&(buf->mutex));
	// end critical section

	return 0;
}

/*
 * Pop data from internal buffer
 * return 0 if success
 */
int popRawDataBuffer(sens_raw_buf_t *buf, sens_raw_pack_t* pack,
		const struct timespec *timeout) {
	if (!buf || !pack)
		return -1;

	// begin critical section
	if (pthread_mutex_lock(&(buf->mutex)))
		return -3;

	// Potential problem present
	// pthread_cond_timedwait must be called in loop while(buf->loaded == 0)
	// But for simplicity and no critical problem if timeout not reached this realization allowed
	if (buf->loaded == 0) {
		cond_timedwait_rel(&(buf->cond), &(buf->mutex), timeout);
	}

	// If no elements for pop
	if (buf->loaded <= 0) {
		pthread_mutex_unlock(&(buf->mutex));
		return -2;
	}

	sens_raw_pack_t *popPtr = buf->packs + buf->first_index;
	memcpy(pack, popPtr, sizeof(*popPtr));

	buf->first_index += 1;
	if (buf->first_index >= buf->capacity)
		buf->first_index = 0;
	buf->loaded -= 1;

	pthread_mutex_unlock(&(buf->mutex));

	return 0;
// end critical section
}

/*
 * Network stream catcher. Resulted data stored at sens_raw_buf_t buffer.
 * Created for use as thread function.
 */
void* netCatch(netcatch_param_t *param) {
	if (!param)
		return NULL;
	if (!param->dataBuf)
		return NULL;

	// UDP receiving buffer
	uint8_t emppPacket[EMPP_MAX_SIZE];

	// Socket object
	int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0)
		return NULL;

#define FINFUNCTION(X) { if (sock) close(sock); puts("Exit from netCatch"); param->status = X; return NULL; }

	// Set socket timeout
	struct timeval tv;
	tv.tv_sec = 1;
	tv.tv_usec = 0;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
		FINFUNCTION(-1);
	}

	// Bind socket
	struct sockaddr_in bindAddr;
	memset(&bindAddr, 0, sizeof(struct sockaddr_in));
	bindAddr.sin_family = AF_INET;
	bindAddr.sin_port = htons(param->port);
	bindAddr.sin_addr.s_addr = htonl(param->saddress);
	if (bind(sock, (struct sockaddr*) &bindAddr, sizeof(bindAddr))) {
		FINFUNCTION(-1);
	}

	puts("Listening...");

	// Prepare for receiving data
	struct sockaddr_in addrFrom;
	socklen_t addrFromLen = sizeof(addrFrom);

	while (param->ctrl == 0) {
		// Get data from socket
		ssize_t recvLen = recvfrom(sock, emppPacket, EMPP_MAX_SIZE, 0,
				(struct sockaddr*) &addrFrom, &addrFromLen);

		if (recvLen < 0) {
			if (errno != EAGAIN) {
				puts("recvfrom internal error");
				continue;
				FINFUNCTION(-1);
			}
			continue;
		}

		// Get packet time stamp
		struct timespec recvTime;
		if (ioctl(sock, SIOCGSTAMPNS, &recvTime)) {
			puts("Failed to get time stamp");
			FINFUNCTION(-1);
		}

		// Check packet parameters
		empp_pack_t *emppPack = (empp_pack_t*) emppPacket;
		// Check packet header size
		if (recvLen < EMPP_HEADER_SIZE) {
			puts("Packet size too short for header");
			continue;
		}
		// Check magic sequence
		if (strncmp(emppPack->header.magic, "EMPP", 4)) {
			puts("Packet magic incorrect");
			continue;
		}
		// Check total packet length
		if (recvLen
				< EMPP_HEADER_SIZE
						+ sizeof(uint16_t) * emppPack->header.params
								* emppPack->header.counts) {
			puts("Packet length incorrect");
			continue;
		}

		// Process packet data
		int res;
		if ((res = pushRawDataBuffer(param->dataBuf, emppPack, &recvTime))) {
			puts("pushRawDataBuffer error");
			continue;
		}

		/*printf("Pack %d, params %d, counts %d time %ld.%09ld\n",
		 emppPack->header.id, emppPack->header.params,
		 emppPack->header.counts, recvTime.tv_sec, recvTime.tv_nsec);*/
	}
	FINFUNCTION(0);

#undef FINFUNCTION
}
