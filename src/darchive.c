/*
 * darchive.c
 *
 *  Created on: May 16, 2017
 *      Author: arhi
 */

#include "darchive.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define ARCH_NAME_TMPL "%04u%02u%02u_%02u%02u.empa"
#define ARCH_NAME_LEN (4+2+2+1+2+2+5)

#define ARCH_SIGNATURE "EMPA"

// Generate file name from time stamp
void printAFname(char *fname, const struct tm *t) {
	sprintf(fname, ARCH_NAME_TMPL, t->tm_year + 1900, t->tm_mon + 1, t->tm_mday,
			t->tm_hour, t->tm_min);
}

/*
 * Write archive header
 * return 0 on success
 */
int writeHeadArchive(archive_t *arch) {
	if (!arch)
		return -1;

	fputs(ARCH_SIGNATURE, arch->f);
	fprintf(arch->f, "%02u%02u", ARCH_VERSION_MAJOR, ARCH_VERSION_MINOR);
	// output parameters description
	fprintf(arch->f, "%02u", arch->params);
	for (int i = 0; i < arch->params; i++)
		fprintf(arch->f, "%-32.32s%-128.128s", arch->pdesc[i].label,
				arch->pdesc[i].description);
	fflush(arch->f);
	return 0;
}

/*
 * Close current and open new archive file base on time t as start time.
 * return 0 on success, negative - otherwise
 */
int reopenFileArchive(archive_t *arch, time_t t) {
	if (!arch)
		return -1;
	if (t < 0)
		return -2;

	if (arch->f) {
		fclose(arch->f);
		arch->f = 0;
	}

	// Get current time if zero time passed
	if (t == (time_t) 0)
		t = time(NULL);
	if (t <= 0)
		return -2;

	// Get current start section rounded time
	time_t ts = (t / (arch->interval * 60u)) * (arch->interval * 60u);

	// Try build file name and check is it exist
	struct tm tm = *(localtime(&ts));

	char fname[sizeof(arch->fname)];
	printAFname(fname, &tm);

	if (!access(fname, F_OK)) {
		// file already exist - drop file name to current time
		tm = *(localtime(&t));
		printAFname(fname, &tm);
	}

	arch->endTime = (t / (arch->interval * 60u) + 1) * (arch->interval * 60u);

	strcpy(arch->fname, fname);
	if (!(arch->f = fopen(fname, "w"))) {
		arch->fname[0] = '\0';
		arch->endTime = 0;
		return -3;
	}

	//printf("Reopen archive %s\n", arch->fname);

	return 0;
}

/*
 *  Initialize archive
 *  return 0 on success
 */
int initArchive(archive_t *arch, const char baseDir[], int interval, int params,
		const param_desc_t pdesc[params]) {
	if (!arch || interval <= 0 || params <= 0)
		return -1;

	// Change base directory
	if (chdir(baseDir))
		return -2;

	// allocate memory and copy parameters description
	arch->pdesc = calloc(params, sizeof(param_desc_t));
	if (arch->pdesc == NULL) {
		return -3;
	}
	memcpy(arch->pdesc, pdesc, sizeof(pdesc[0]) * params);
	arch->params = params;
	arch->interval = interval;

	arch->f = (FILE*) 0;
	if (reopenFileArchive(arch, (time_t) 0)) {
		closeArchive(arch);
		return -2;
	}

	// Write file header
	if (writeHeadArchive(arch)) {
		closeArchive(arch);
		return -4;
	}

	return 0;
}

/*
 * Close files and reset time stamps
 */
void closeArchive(archive_t *arch) {
	if (!arch)
		return;

	if (arch->f) {
		fclose(arch->f);
		arch->f = NULL;
	}

	if (arch->pdesc) {
		free(arch->pdesc);
		arch->pdesc = NULL;
	}

	arch->endTime = 0;
	arch->fname[0] = '\0';
}

/*
 * Archiving function
 */
void* archiveData(archiving_param_t *param) {
	if (param == NULL)
		return NULL;
	if (param->dataBuf == NULL || param->archive == NULL) {
		param->status = -1;
		return NULL;
	}

	size_t sampleSize = sample_size(param->dataBuf);
	uint8_t samplePH[sampleSize];
	data_sample_t *sample = (data_sample_t*) samplePH;

	while (param->ctrl == 0) {
		// Wait data for write
		struct timespec timeout = { .tv_sec = 5, .tv_nsec = 0 };
		int samplesToWrite = waitHalfSampleDataBuffer(param->dataBuf, &timeout);
		if (samplesToWrite <= 0) {
			continue;
		}
		for (int i = 0; i < samplesToWrite; i++) {
			// Get data from data buffer
			if (popSampleDataBuffer(param->dataBuf, sample, NULL))
				continue;
			if (sample->sec >= param->archive->endTime) {
				reopenFileArchive(param->archive, (time_t) sample->sec);
				writeHeadArchive(param->archive);
			}
			// write sample to file
			fwrite(sample, sampleSize, 1, param->archive->f);
		}
		fflush(param->archive->f);
	}

	closeArchive(param->archive);

	param->status = 0;
	puts("Exit from data archiving");

	return NULL;
}
