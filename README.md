# Catcher and first stage calculator for Empala data stream

Catch UDP data flow from Empala acquisition system, calculate perameters
according model, write results to archive file.

Package format of network stream verion 0.1.

| Offset            | Name   | Type     | Description                        |
| ----------------- | ------ | -------- | ---------------------------------- |
| 0                 | magic  | char[4]  | 'EMPP' start sequence              |
| 4                 | period | uint32_t | Current base period of 1 MHz timer |
| 8                 | params | uint16_t | Parameters in sample               |
| 10                | id     | uint16_t | Package number                     |
| 12                | counts | uint16_t | Samples in package                 |
| 14+2*(i*params+N) | pN     | uint16_t | ParamN data for sample i           |